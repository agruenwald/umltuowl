package argouml_xmi_34;


/**
 * 
 * @author Andreas Gruenwald <a.gruenw@gmail.com>.
 * Test cases for reference example models.
 *
 */
public abstract class ArgoUMLDefaultAbstractClass  {
	protected final static String ARGOUML_SAMPLES = "argouml_xmi_3_4/input-file.xmi";
	public final static String CONVERTER_NAME = "ArgoUMLConverterXMIV034";	
}
