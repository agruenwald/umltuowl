package v_paradigm_xmi_2_1;

import metamodel.DefaultAttributeTest;

import org.junit.BeforeClass;

import testhelper.TestHelper;

/**
 * 
 * @author Andreas Gruenwald <a.gruenw@gmail.com>.
 * Test cases for reference example models.
 *
 */
public class VParadigmDefaultAttributeTest extends DefaultAttributeTest {
	/**
	 * Load the meta model at the beginning.
	 * @throws Exception
	 */
	@BeforeClass
	public static void loadMetaModel() throws Exception {
		metaModel = TestHelper.loadMetaModel(VParadigmDefaultAbstractClass.CONVERTER_NAME,
											 VParadigmDefaultAbstractClass.VISUAL_PARADIGM_SAMPLES);
	}

}
