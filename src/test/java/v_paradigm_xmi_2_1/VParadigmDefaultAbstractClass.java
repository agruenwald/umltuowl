package v_paradigm_xmi_2_1;


/**
 * 
 * @author Andreas Gruenwald <a.gruenw@gmail.com>.
 * Test cases for reference example models.
 *
 */
public abstract class VParadigmDefaultAbstractClass  {
	protected final static String VISUAL_PARADIGM_SAMPLES = "v_paradigm_xmi_2_1/input-file.xmi";
	public final static String CONVERTER_NAME = "VisualParadigmConverterXMI2";

}
