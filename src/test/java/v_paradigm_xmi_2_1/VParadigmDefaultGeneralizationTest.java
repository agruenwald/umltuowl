package v_paradigm_xmi_2_1;

import metamodel.DefaultGeneralizationTest;

import org.junit.BeforeClass;

import testhelper.TestHelper;

/**
 * 
 * @author Andreas Gruenwald <a.gruenw@gmail.com>.
 * Test cases for reference example models.
 *
 */
public class VParadigmDefaultGeneralizationTest extends DefaultGeneralizationTest  {
	/**
	 * Load the meta model at the beginning.
	 * @throws Exception
	 */
	@BeforeClass
	public static void loadMetaModel() throws Exception {
		metaModel = TestHelper.loadMetaModel(VParadigmDefaultAbstractClass.CONVERTER_NAME,
											 VParadigmDefaultAbstractClass.VISUAL_PARADIGM_SAMPLES);
	}

}
