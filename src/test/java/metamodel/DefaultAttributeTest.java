package metamodel;

import static org.hamcrest.Matchers.either;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;

import org.junit.Test;

import at.tuvienna.metamodel.MetaClass;
import at.tuvienna.metamodel.MetaModel;
/**
 * 
 * @author Andreas Gruenwald <a.gruenw@gmail.com>.
 * Test cases for reference example models. Same for all different converters.
 * Tests if attributes are converted correctly for default reference models.
 * Should be used for every converter. See {@code VParadigmDefaultAttributeTest}.
 *
 */
public abstract class DefaultAttributeTest {			
	protected static MetaModel metaModel;
	
	private final static String PACKAGE_1 = "Attributes test";

	protected MetaClass readClass(String classname) {
		return metaModel.findPackageByName(PACKAGE_1).findClassByName(classname);
	}
	
	@Test
	public void studentClass() {
		assertNotNull(readClass("Student"));
	}	

	@Test
	public void buildingClass() {		
		assertNotNull(readClass("Building"));
	}

	@Test
	public void studentAge() {		
		assertNotNull(readClass("Student").findAttributeByName("age"));
	}

	@Test
	public void studentName() {		
		assertNotNull(readClass("Student").findAttributeByName("name"));
	}

	@Test
	public void studentMale() {		
		assertNotNull(readClass("Student").findAttributeByName("male"));
	}
	
	@Test
	public void studentAverageGrade() {		
		assertNotNull(readClass("Student").findAttributeByName("averageGrade"));
	}
	
	@Test
	public void studentNegAverageGrade() {		
		assertNull(readClass("Student").findAttributeByName("agex"));
	}
	
	@Test
	public void buildingName() {		
		assertNotNull(readClass("Building").findAttributeByName("name"));
	}
	
	@Test
	public void buildingAge() {		
		assertNotNull(readClass("Building").findAttributeByName("age"));
	}
	
	@Test
	public void studentAgeDatatype() {
		assertThat(readClass("Student")
				.findAttributeByName("age").getRange().toLowerCase(),
				either(equalTo("integer"))
				   .or(equalTo("int")));
	}

	@Test
	public void studentNameDatatype() {
		assertThat(readClass("Student")
				.findAttributeByName("name").getRange().toLowerCase(),
				(equalTo("string")));
	}

	@Test
	public void studentMaleDatatype() {
		assertThat(readClass("Student")
				.findAttributeByName("male").getRange().toLowerCase(),
				either(equalTo("bool")).or(equalTo("boolean")));
	}

	@Test
	public void studentAverageGradeDatatype() {
		assertThat(readClass("Student")
				.findAttributeByName("averageGrade").getRange().toLowerCase(),
				(equalTo("double")));
	}
	
	@Test
	public void buildingNameDatatype() {
		assertThat(readClass("Building")
				.findAttributeByName("name").getRange().toLowerCase(),
				(equalTo("string")));
	}

	@Test
	public void buildingAgeDatatype() {
		assertThat(readClass("Building")
				.findAttributeByName("age").getRange().toLowerCase(),
				(equalTo("string")));
	}

	@Test
	public void studentVisibility() {
		MetaClass student = readClass("Student");
		assertThat(student.findAttributeByName("age").getVisibility(),equalTo("private"));
		assertThat(student.findAttributeByName("name").getVisibility(),equalTo("private"));
		assertThat(student.findAttributeByName("male").getVisibility(),equalTo("private"));
		assertThat(student.findAttributeByName("averageGrade").getVisibility(),equalTo("private"));
	}
	
	@Test
	public void buildingVisibility() {
		MetaClass student = readClass("Building");
		assertThat(student.findAttributeByName("age").getVisibility(),equalTo("private"));
		assertThat(student.findAttributeByName("name").getVisibility(),equalTo("private"));
	}
	
}
