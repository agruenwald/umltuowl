package argouml_xmi_2_1;


/**
 * 
 * @author Andreas Gruenwald <a.gruenw@gmail.com>.
 * Test cases for reference example models.
 *
 */
public abstract class ArgoUMLDefaultAbstractClass  {
	protected final static String ARGOUML_SAMPLES = "argouml_xmi_2_1/input-file.xmi";
	protected final static String CONVERTER_NAME = "ArgoUMLConverterXMI2";	
}
