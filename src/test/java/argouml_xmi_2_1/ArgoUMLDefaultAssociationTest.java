package argouml_xmi_2_1;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertNull;

import java.util.List;

import metamodel.DefaultAssociationTest;

import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import at.tuvienna.metamodel.MetaAssociation;
import at.tuvienna.metamodel.MetaClass;

import testhelper.TestHelper;

/**
 * 
 * @author Andreas Gruenwald <a.gruenw@gmail.com>.
 * Test cases for reference example models.
 *
 */
public class ArgoUMLDefaultAssociationTest extends DefaultAssociationTest  {
	/**
	 * ArgoUML does not support different packages (for XMI export).
	 */
	@Override
	protected MetaClass readClass(String classname) {
		List<MetaClass> classes =  metaModel.findPackageByName("").findMultipleClassesByName(classname);
		if(classes.isEmpty()) {
			return null;
		} else if (classes.size() == 1) {
			return classes.get(0);
		} else {
			//prefer classes with associations because in Argo UML it can be that 
			//more than a single class exist with same class name.
			for(MetaClass c : classes) {
				if(!c.getAssociations().isEmpty()) {
					return c;
				}
			}
			return classes.get(0);
		}
	}
	
	/**
	 * Load the meta model at the beginning.
	 * @throws Exception
	 */
	@BeforeClass
	public static void loadMetaModel() throws Exception {
		metaModel = TestHelper.loadMetaModel(ArgoUMLDefaultAbstractClass.CONVERTER_NAME,
											 ArgoUMLDefaultAbstractClass.ARGOUML_SAMPLES);
	}
	
	/** ArgoUML does not support comments on associations. A documentation field exists in
	 * Argo, but the content is not exported into the XMI file.
	 */
	@Ignore
	@Override
	@Test
	public void studentExamComment() {
		
	}
	
	/** ArgoUML does not support comments on associations. A documentation field exists in
	 * Argo, but the content is not exported into the XMI file.
	 */
	@Ignore
	@Override
	@Test
	public void trunkTreeAssociationComment() {
		
	}
	
	/** ArgoUML does not support comments on associations. A documentation field exists in
	 * Argo, but the content is not exported into the XMI file.
	 */
	@Ignore
	@Override
	@Test
	public void treeTrunkAssociationComment() {
		
	}
	
	/**
	 * Additional association navigation test only for ArgoUML.
	 */	
	@Test
	public void salesManagerCustomerIsNavigable() {
		MetaAssociation a = readClass("Sales Manager").findAssociationByName("Customer");
		assertThat(a.getTo().getRange().get(0),is(equalTo("1")));
		assertThat(a.getTo().getRange().get(1),is(equalTo("*")));
	}

	/**
	 * Additional association navigation test only for ArgoUML.
	 */	
	@Test
	public void notCustomerSalesManagerIsNavigable() {
		MetaAssociation a = readClass("Customer").findAssociationByName("Sales Manager");
		assertNull(a);
	}

}
