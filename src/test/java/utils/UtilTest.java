package utils;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

import java.util.List;

import org.junit.Test;

import at.tuvienna.utils.TransformationException;
import at.tuvienna.utils.Util;

/**
 * 
 * @author Andreas Gruenwald <a.gruenw@gmail.com>.
 * Test cases for utility class.
 *
 */
public class UtilTest {
	
	@Test
	public void testNormalizeRange1() throws TransformationException {
		List<String> list = Util.normalizeRange("0", "*");
		assertThat(list.size(),equalTo(2));
		assertThat(list.get(1),equalTo("*"));
	}
	
	@Test
	public void testNormalizeRange2() throws TransformationException {
		List<String> list = Util.normalizeRange("", "*");
		assertThat(list.size(),equalTo(2));
		assertThat(list.get(1),equalTo("*"));
	}

	@Test
	public void testNormalizeRange3() throws TransformationException {
		List<String> list = Util.normalizeRange("", "");
		assertThat(list.size(),equalTo(1));
	}
	
	@Test
	public void testNormalizeRange4() throws TransformationException {
		List<String> list = Util.normalizeRange("1,2,3,4", "1,2,3,4");
		assertThat(list.size(),equalTo(4));
		assertThat(list.get(0),equalTo("1"));		
		assertThat(list.get(3),equalTo("4"));
	}

	@Test
	public void testNormalizeRange5() throws TransformationException {
		List<String> list = Util.normalizeRange("0..*", "0..*");
		assertThat(list.size(),equalTo(2));
		assertThat(list.get(0),equalTo("0"));		
		assertThat(list.get(1),equalTo("*"));
	}
	
	@Test
	public void testNormalizeRange6() throws TransformationException {
		List<String> list = Util.normalizeRange("2,3..10,11", "2,3..10,11");
		assertThat(list.size(),equalTo(10));
		assertThat(list.get(0),equalTo("2"));		
		assertThat(list.get(5),equalTo("7"));
		assertThat(list.get(9),equalTo("11"));
	}
	
	@Test(expected = TransformationException.class)
	public void negTestNormalizeRange1() throws TransformationException {
		Util.normalizeRange("2,x", "2,x");
	}
	
}
