package ms_visio_2010_xmi_1_0;

import metamodel.DefaultGeneralizationTest;

import org.junit.BeforeClass;

import testhelper.TestHelper;

/**
 * 
 * @author Andreas Gruenwald <a.gruenw@gmail.com>.
 * Test cases for reference example models.
 *
 */
public class MSVisio2010XMI1DefaultGeneralizationTest extends DefaultGeneralizationTest  {
	/**
	 * Load the meta model at the beginning.
	 * @throws Exception
	 */
	@BeforeClass
	public static void loadMetaModel() throws Exception {
		metaModel = TestHelper.loadMetaModel(MSVisio2010XMI1DefaultAbstractClass.CONVERTER_NAME,
											 MSVisio2010XMI1DefaultAbstractClass.SAMPLES_FILE);
	}

}
