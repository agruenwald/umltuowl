package json;

import static org.junit.Assert.*;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.util.Scanner;
import java.io.File;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import at.tuvienna.metamodel.MetaModel;
import at.tuvienna.metamodeltoowl.JsonMaker;

import testhelper.TestHelper;

public class JsonTest {
	private static MetaModel metamodel = null;
	
	private String getTestFolder() {
		String url=ClassLoader.getSystemResource("reference-models/").getPath();
		url = url.replace("/bin/reference-models/","/test/reference-models/json/");
		if(!url.contains("json")) {
			url += "json/";
		}
		return url;
	}
	
	private String readFileContent(String filePath) throws IOException {
		 FileInputStream stream = new FileInputStream(new File(filePath));
		  try {
		    FileChannel fc = stream.getChannel();
		    MappedByteBuffer bb = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size());
		    /* Instead of using default, pass in a decoder. */
		    return Charset.defaultCharset().decode(bb).toString();
		  }
		  finally {
		    stream.close();
		  }
	}
	
	@Before
	public void setUp() throws Exception {
		metamodel = TestHelper.loadMetaModel(v_paradigm_xmi_2_1.VParadigmDefaultAbstractClass.CONVERTER_NAME, 
				"itpm-example/itpm.uml", "itpm-example/settings-itpm.properties");
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testToJsonRun() {
		String res = JsonMaker.toJson(metamodel);
		assertFalse(res.isEmpty());
	}
	
	@Test
	public void testToMetaModelRun() {
		String json = JsonMaker.toJson(metamodel);
		MetaModel mm = JsonMaker.toMetaModel(json);
		assertTrue(mm.getPackages().size()>0);
	}
	
	@Test
	public void testToFile() throws IOException {
		String url = getTestFolder() + "output.json";
		System.out.println(url);
		JsonMaker.toJsonFile(metamodel, url);
		assertTrue(true);
	}
	
	@Test
	public void testRoundTrip() {
		//System.out.println(metamodel);
		String json = JsonMaker.toJson(metamodel);
		//System.out.println(json);
		System.out.println(json);
		MetaModel m2 = JsonMaker.toMetaModel(json);
		//System.out.println(m2);
	}
	
	@Test
	public void testBackToMetaModelWeb1() throws IOException {
		String url = getTestFolder() + "web_diagram_itpm1.json";
		String json = this.readFileContent(url);
		MetaModel mm = JsonMaker.toMetaModel(json);
		//System.out.println(mm);
		assertTrue(mm.getPackages().size()>0);
		assertTrue(mm.getPackages().iterator().next().getClasses().size()>5);
//TODO depends on which meta-model is read		assertTrue(mm.getPackages().iterator().next().getClasses().iterator().next().getAttributes().size()>3);
	}
	
	

}
