* Nothing in here yet. The serialization from the metamodel to json 
* and reverse is automated. The aim of the JSON serialization format
* is to bridge Java and JSON. Hence, tests need to include the 
* serialization of the meta model from JavaScript objects, and then
* forward-processing of this serialized format into the Java meta model.