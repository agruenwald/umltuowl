package at.tuvienna.uml2metamodel;

import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import at.tuvienna.main.XMLPreprocessor;
import at.tuvienna.metamodel.MetaAssociation;
import at.tuvienna.metamodel.MetaAttribute;
import at.tuvienna.metamodel.MetaClass;
import at.tuvienna.metamodel.MetaClassPoint;
import at.tuvienna.metamodel.MetaModel;
import at.tuvienna.metamodel.MetaPackage;
import at.tuvienna.utils.Util;


/**
 * @author Andreas Gruenwald
 * Converter for Argo UML XMI2.0 syntax version 0.34.
 * Note: Does not work for version 0.32!!!
 */
public class ArgoUMLConverterXMIV034 extends UMLConverter {
	private MetaModel model;
	private Document xmi;
	private static final Logger logger = Logger.getLogger(ArgoUMLConverterXMIV034.class);

	/**
	 * Creation of a new Argo UML XMI 2.0 metamodel (done automatically by factory).
	 */
	public ArgoUMLConverterXMIV034() {
		this.model = new MetaModel("ARGOUML_XMI_2_0_MODEL_V0_34");
	}
	
	@Override
	public MetaModel convertModel(Document xmi) {
		logger.info(String.format("Convert model using %s XMI converter.",ArgoUMLConverterXMIV034.class.getSimpleName()));
		this.xmi = xmi;
		this.model.setName(xmi.select("uml|Model").attr("name"));
		Elements classes = xmi.select("UML|Class[name]");
		Elements interfaces = xmi.select("UML|Interface[name]");
		buildClasses(classes);
		buildInterfaces(interfaces);
		addGeneralization(classes);
		addGeneralization(interfaces);
		addInterfaceRealization(interfaces);
		addAssociations();
		addComments(classes,interfaces);
		return model;
	}
	
	/**
	 * Bind UML comments to classes.
	 */
	private void addComments(Elements classes, Elements interfaces) {
		logger.trace("Called addComments(classes,interfaces).");
		
		for(Element c : classes) {
			Elements comments = c.select("UML|ModelElement[comment]").select("UML|Comment");
			for (Element com : comments) {
				String commentId=com.attr("idref");
				String comment = xmi.select(String.format("UML|Comment[id=%s]",commentId)).attr("body");
				MetaClass metaClass = model.findClassById(c.attr("id"));
				metaClass.getComment().add(comment);
				logger.trace(String.format("Added comment %s to %s.",comment,metaClass));
			}
		}
		
		for(Element c : interfaces) {
			Elements comments = c.select("UML|ModelElement[comment]").select("UML|Comment");
			for (Element com : comments) {
				String commentId=com.attr("idref");
				String comment = xmi.select(String.format("UML|Comment[id=%s]",commentId)).attr("body");
				MetaClass metaClass = model.findClassById(c.attr("id"));
				metaClass.getComment().add(comment);
				logger.trace(String.format("Added comment %s to %s.",comment,metaClass));
			}
		}
	}
	
	/**
	 * Add sub- and super class relations.
	 * @param classes identified classes (and interfaces).
	 */
	private void addGeneralization(Elements classes) {
		logger.trace("Called addGeneralization(classes).");
		for(Element c : classes) {
			String classId = c.attr("id");
			MetaClass clazz = model.findClassById(classId);
			logger.trace(String.format("Inspect class %s (generalization).",clazz));
			Elements x = xmi.select("UML|Generalization[child]");
			for(Element e : x) {
				Elements es = e.select(String.format("UML|Class[idref=%s]",classId));
				if(es.size()!=0) {
					String superClassId = e.parent().select("UML|Generalization[parent]").select("UML|Class").first().attr("idref");
					MetaClass superClazz = model.findClassById(superClassId);
					if(superClazz == null) {
						logger.warn("========>"+superClassId+" (Interface?) ");
					} else {
						logger.debug(String.format("Class %s has superclass %s.",clazz,superClazz));
						superClazz.getSubclasses().add(clazz);
						clazz.getSuperclasses().add(superClazz);
					}
				}
			}		
		}				
	}
	
	/**
	 * Add sub- and super class relations for interfaces.
	 */
	public void addInterfaceRealization(Elements interfaces) {
		logger.trace("Called addInterfaceRealization(interfaces).");
		for(Element c : interfaces) {
			String classId = c.attr("id");
			logger.trace(String.format("Inspect class %s (abstraction).",classId));
			Elements x = xmi.select("UML|Abstraction[id]");
			for(Element e : x) {
				Elements es = e.select(String.format("UML|Interface[idref=%s]",classId));
				if(es.size()!=0) {
					String subClassId = e.select("UML|Dependency[client]").first().child(0).attr("idref");
					String superClassId = e.select("UML|Dependency[supplier]").first().child(0).attr("idref");
					MetaClass superClazz = model.findClassById(superClassId);
					MetaClass clazz = model.findClassById(subClassId);
					if(superClazz == null || clazz == null) {
						logger.warn("========>"+superClassId+"/" + clazz + " couldn't be realized.");
					} else {
						logger.debug(String.format("Class %s has superclass %s.",clazz,superClazz));
						superClazz.getSubclasses().add(clazz);
						clazz.getSuperclasses().add(superClazz);
					}
				}
			}		
		}	 
	}
	
	/**
	 * Add fine grained information (attributes) to meta classes.
	 * @param classes identified classes and interfaces.
	 */
	private void buildClasses(Elements classes) {
		logger.trace("Called buildClasses(classes).");
		for(Element c : classes) {
			String name = c.attr("name");
			if (Util.isSupportedPrimitiveDatatype(name)) {
				logger.warn(String.format("Class %s has been detected as a datatype primitive and therefore was removed from class list.",name));
			} else if (name.isEmpty()) {
							
			} else {
				boolean isAbstract = false;
				if(name.contains("{abstract}")
				|| c.attr("isAbstract").equalsIgnoreCase("true")) {
					isAbstract = true;
					name = name.replace("{abstract}","").trim();
					logger.info(String.format("Class %s was classified as abstract.",name));
				}
				MetaClass umlClass = new MetaClass(name);
				umlClass.setId(c.attr("id"));
				umlClass.setAbstractClass(isAbstract);			
				umlClass = buildAttributes(umlClass,c);
				MetaPackage metaPackage = model.managePackageByName("");
				metaPackage.getClasses().add(umlClass);							
			}
		}
	}

	/**
	 * Extract interfaces from XML model and add it to meta model as classes.
	 * @param interfaces identified interfaces (XMI).
	 */
	private void buildInterfaces(Elements interfaces) {
		logger.trace("Called buildInterfaces(interfaces).");
		for(Element c : interfaces) {
			String name = c.attr("name");			
			MetaClass umlClass = new MetaClass(name);
			umlClass.setId(c.attr("id"));
			umlClass.setInterfaceClass(true);
			MetaPackage metaPackage = model.managePackageByName("");
			metaPackage.getClasses().add(umlClass);									
		}		
	}
	
	/**
	 * find data type by its relating {@code rangeId}, which is part of the XMI file.
	 * @param metaClass the meta class the attribute range belongs to.
	 * @param rangeId the internal data type id (provided by VParadigm XMI).
	 * @return data type name (e.g. string) or empty if void.
	 */
	private String findRangeType(MetaClass metaClass, String rangeId) {
		logger.trace(String.format("Called findRangeType(xmi-document,%s",rangeId));
		Elements e = xmi.select("packagedElement[xmi:type=uml:DataType]")
				   .select(String.format("[xmi:id=%s]",rangeId));
		
	
		if(e.first() == null) {
			e = xmi.select(String.format("[xmi:id=%s]",rangeId));
			if(e.first() != null) {
				 if(Util.isSupportedPrimitiveDatatype(e.first().attr("name"))) {
					 logger.warn(String.format("Did not find data type \"%s\" for class %s (range id %s). " +
								"However, attribute name has been detected as an primitive datatype, so the problem has been solved automatically by the converter. "
								,e.first().attr("name"),metaClass.getName(),rangeId));
				 } else {
					logger.warn(String.format("Did not find data type for class %s (range id %s). " +
							"The data range seems not to be a primitive data type, so the datatype was removed."
							,metaClass.getName(),rangeId,e.first().attr("name")));
					return "void";
				 }
			}
		}		
		return e.first().attr("name");
	}
	
	/**
	 * Add all attributes to meta class. Methods are ignored.
	 * @param umlClass a single meta class.
	 * @param xmiClass corresponding class from Visual Paradigm XMI file.
	 * @return meta class that contains all related attributes.
	 */
	private MetaClass buildAttributes(MetaClass umlClass, Element xmiClass) {
		logger.trace(String.format("Called buildAttributes(%s,jsoupXMIClass)",umlClass.getName()));
		Elements attrs = xmiClass.select("uml|Attribute");
		for (Element a : attrs) {
			if(!a.attr("association").isEmpty()) {
				logger.trace(String.format("Association %s found as attribute (skipped).",a.attr("association")));
				continue;
			}
			MetaAttribute metaAttribute = new MetaAttribute(a.attr("name"));
			metaAttribute.setVisibility(a.attr("visibility"));
			String rangeId = a.attr("type");
			String range = "void";
			if(!rangeId.isEmpty()) {
				logger.debug("Range id of " + umlClass + " is " + rangeId);
				range = findRangeType(umlClass,rangeId);
			} else {
				String idref = a.select("UML|DataType[idref]").attr("idref");
				if(!idref.isEmpty()) {
					logger.debug("Range id of " + umlClass + " is " + idref);
					range = this.xmi.select("uml|DataType[id="+idref+"]").attr("name");				
				}
			}
			metaAttribute.setRange(range);
			umlClass.getAttributes().add(metaAttribute);
		}
		return umlClass;
	}
	
	/**
	 * Add associations between classes.
	 */
	private void addAssociations() {
		Elements associations = xmi.select("uml|Association");
		for(Element ass : associations) {
			Elements elements = ass.select("uml|AssociationEnd[id]");
			logger.debug("Found new association between classes.");
			String name = ass.attr("name"); 
			String id 	= ass.attr("id"); //ass.attr("memberEnd"); //if name exists
			if(id.isEmpty() && name.isEmpty()) {
				continue;
			}
			logger.debug("id and name: " + id + "-" + name);
			if(elements.size() != 2) {
				logger.fatal(String.format("Wrong file format. Association %s contains more (or less) than 2 (i.e. %d) endpoints. Please check your XMI file or send us an e-mail.",name,elements.size()));
			} else {
				this.createAndLinkAssociation(id,elements,ass,name);
			}  
		}
	}	
	
	/**
	 * Creates new associations and links them to the meta classes.
	 * @param associationId the ID of the association (same for both link directions). 
	 * 		  Is used to identify the association globally (e.g. find comments). 
	 * @param elements a list of XMI elements containing end points (=XMI code).
	 * @param ass the association element (XMI) that contains the two endpoints and
	 * 		  navigation information.
	 * @param name name of the association if user named it, otherwise empty.
	 */
	private void createAndLinkAssociation(String associationId, Elements elements, Element ass, String name) {
		String lowerValueId   = elements.get(0).attr("id");
		String upperValueId   = elements.get(1).attr("id");
		String lowerValueFrom = elements.get(0).select("UML|MultiplicityRange").attr("lower");
		String upperValueFrom = elements.get(0).select("UML|MultiplicityRange").attr("upper");
		String lowerValueTo   = elements.get(1).select("UML|MultiplicityRange").attr("lower");
		String upperValueTo   = elements.get(1).select("UML|MultiplicityRange").attr("upper");
		
		lowerValueFrom = lowerValueFrom.equals("-1") ? "*" : lowerValueFrom;
		upperValueFrom = upperValueFrom.equals("-1") ? "*" : upperValueFrom;
		lowerValueTo = lowerValueTo.equals("-1") ? "*" : lowerValueTo;
		upperValueTo = upperValueTo.equals("-1") ? "*" : upperValueTo;
		
		String aggregationFrom  = elements.get(0).attr("aggregation").equals("aggregate") ? "true" : "false";
		String aggregationTo    = elements.get(1).attr("aggregation").equals("aggregate") ? "true" : "false";
		String compositionFrom  = elements.get(0).attr("aggregation").equals("composite") ? "true" : "false";
		String compositionTo    = elements.get(1).attr("aggregation").equals("composite") ? "true" : "false";
	
		MetaClass metaClassFrom = this.model.findClassById(elements.get(0).select("UML|AssociationEnd[participant]").first().child(0).attr("idref"));
		MetaClass metaClassTo   = this.model.findClassById(elements.get(1).select("UML|AssociationEnd[participant]").first().child(0).attr("idref"));				
		
		//break if class has not been found
		if(metaClassFrom == null || metaClassTo == null) {
			logger.warn(String.format("One of the association endpoins contains an empty class so we skipped the association (association ID %s - name %s).",associationId,name));
			return;
		}
	
		boolean isBidirectional = true;
		boolean fromToNavigable = elements.get(0).attr("isNavigable").equals("true") ? true : false;
		boolean toFromNavigable = elements.get(1).attr("isNavigable").equals("true") ? true : false;
		
		if(fromToNavigable && toFromNavigable) {
			
		} else {
			isBidirectional = false;
		}
		
		
		logger.debug(String.format("association (%s) %s<->%s (%s;%s) found. Navigation is %s.",
						name.isEmpty() ? "noname" : name,
						metaClassFrom.getName(),metaClassTo.getName(),lowerValueFrom,upperValueFrom,
						isBidirectional ? "bidirectional" : "unidirectional"));
		
		MetaAssociation fromAssociation = new MetaAssociation(associationId, lowerValueId,name);			
		MetaAssociation toAssociation   = new MetaAssociation(associationId, upperValueId,name);
		
		fromAssociation.setFrom(new MetaClassPoint(metaClassFrom));
		fromAssociation.setTo(new MetaClassPoint(metaClassTo));
		fromAssociation.getFrom().setRangeSmart(lowerValueFrom, upperValueFrom);
		fromAssociation.getTo().setRangeSmart(lowerValueTo, upperValueTo);
		fromAssociation.setAggregation(Boolean.parseBoolean(aggregationFrom));
		fromAssociation.setComposition(Boolean.parseBoolean(compositionFrom));
		
		toAssociation.setTo(fromAssociation.getFrom());
		toAssociation.setFrom(fromAssociation.getTo());				
		toAssociation.setAggregation(Boolean.parseBoolean(aggregationTo));
		toAssociation.setComposition(Boolean.parseBoolean(compositionTo));
		
		if(isBidirectional) {
			fromAssociation.setInverseAssociation(toAssociation);
			toAssociation.setInverseAssociation(fromAssociation);
			metaClassFrom.getAssociations().add(fromAssociation);
			metaClassTo.getAssociations().add(toAssociation);
		} else if (toFromNavigable) {
			metaClassFrom.getAssociations().add(fromAssociation);
		} else {
			metaClassTo.getAssociations().add(toAssociation);
		}
	}
}
