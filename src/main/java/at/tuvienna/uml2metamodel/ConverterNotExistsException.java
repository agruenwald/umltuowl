package at.tuvienna.uml2metamodel;
/**
 * 
 * @author Andreas Gruenwald <a.gruenw@gmail.com>
 * Is thrown if a converter or file format is used that is unknown.
 */
public class ConverterNotExistsException extends Exception {
	private static final long serialVersionUID = 7260867439710065901L;
	
	public ConverterNotExistsException(String msg) {
		super(msg);
	}
}
