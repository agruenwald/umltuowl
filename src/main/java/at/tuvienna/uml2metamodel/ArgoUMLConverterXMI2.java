package at.tuvienna.uml2metamodel;

import java.util.LinkedList;
import java.util.List;


import org.apache.log4j.Logger;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import at.tuvienna.main.XMLPreprocessor;
import at.tuvienna.metamodel.MetaAssociation;
import at.tuvienna.metamodel.MetaAttribute;
import at.tuvienna.metamodel.MetaClass;
import at.tuvienna.metamodel.MetaClassPoint;
import at.tuvienna.metamodel.MetaModel;
import at.tuvienna.metamodel.MetaPackage;
import at.tuvienna.utils.Util;


/**
 * @author Andreas Gruenwald
 * Converter for Argo UML XMI2.0 syntax.  
 */
public class ArgoUMLConverterXMI2 extends UMLConverter {
	private MetaModel model;
	private Document xmi;
	private static final Logger logger = Logger.getLogger(ArgoUMLConverterXMI2.class);

	/**
	 * Creation of a new Argo UML XMI 2.0 metamodel (done automatically by factory).
	 */
	public ArgoUMLConverterXMI2() {
		this.model = new MetaModel("ARGOUML_XMI_2_0_MODEL");
	}
	
	@Override
	public MetaModel convertModel(Document xmi) {
		logger.info(String.format("Convert model using %s XMI converter.",ArgoUMLConverterXMI2.class.getSimpleName()));
		this.xmi = xmi;
		this.model.setName(xmi.select("uml|Model").attr("name"));
		Elements classes = xmi.select("[xmi:type=uml:Class][name]");
		Elements interfaces = xmi.select("[xmi:type=uml:Interface][name]");
		buildClasses(classes);
		buildInterfaces(interfaces);
		addGeneralization(classes);
		addGeneralization(interfaces);
		addInterfaceRealization(interfaces);
		addAssociations();
		addComments();
		return model;
	}
	
	/**
	 * Bind UML comments to classes.
	 */
	private void addComments() {
		logger.trace("Called addComments(xmi,classes).");
		Elements comments = xmi.select("ownedComment");		
		for(Element c : comments) {
			String comment = c.select(XMLPreprocessor.XMI_BODY).text();
			logger.trace(String.format("Found comment %s.",comment));
			String elementId = c.attr("annotatedElement");

			if(!elementId.isEmpty()) {					
				MetaClass metaClass = model.findClassById(elementId);
				if(metaClass == null) {
					//Comment for association
					List<MetaAssociation> assList = model.findAssociationById(elementId);
					if(!assList.isEmpty()) {
						for(MetaAssociation ass : assList) {
							ass.getComment().add(comment);
							logger.trace(String.format("Added comment %s to %s.",comment,ass));
						}
					} else {
						logger.error(String.format("addComments(): Could not find class with id %s",elementId));
					}
				} else {
					metaClass.getComment().add(comment);
					logger.trace(String.format("Added comment %s to %s.",comment,metaClass));
				}				
			}					
		}
	}
	
	/**
	 * Add sub- and super class relations.
	 * @param classes identified classes (and interfaces).
	 */
	private void addGeneralization(Elements classes) {
		logger.trace("Called addGeneralization(classes).");
		for(Element c : classes) {
			Elements generalization = c.select("generalization");
			String classId 	    = c.attr("xmi:id");
			MetaClass clazz = model.findClassById(classId);
			logger.trace(String.format("Inspect class %s (generalization).",clazz));
			for(Element superclass : generalization) {				
				String superClassId = superclass.attr("general");
				MetaClass superClazz = model.findClassById(superClassId);
				if(superClazz == null) {
					logger.warn("========>"+superClassId+" (Interface?) ");
				} else {
					logger.debug(String.format("Class %s has superclass %s.",clazz,superClazz));
					superClazz.getSubclasses().add(clazz);
					clazz.getSuperclasses().add(superClazz);
				}
			}						
			
		}				
	}
	
	/**
	 * Add sub- and super class relations for interfaces.
	 */
	public void addInterfaceRealization(Elements interfaces) {
		for(Element interf : interfaces) {
			String id = interf.attr("xmi:id");
			Elements interfaceRealizations = 
				xmi.select(String.format
						("interfaceRealization[xmi:type=uml:InterfaceRealization][supplier=%s][contract=%s]",
								id,id));
			for(Element i : interfaceRealizations) {
				logger.info(String.format("Found interface implementation %s",i.parent().attr("name")));
				MetaClass interfaceClass = this.model.findClassById(id);
				MetaClass subclass = model.findClassById(i.parent().attr("xmi:id"));
				interfaceClass.getSubclasses().add(subclass);
				subclass.getSuperclasses().add(interfaceClass);
			}
		}	 
	}
	
	/**
	 * Add fine grained information (attributes) to meta classes.
	 * @param classes identified classes and interfaces.
	 */
	private void buildClasses(Elements classes) {
		logger.trace("Called buildClasses(classes).");
		for(Element c : classes) {
			String name = c.attr("name");
			if (Util.isSupportedPrimitiveDatatype(name)) {
				logger.warn(String.format("Class %s has been detected as a datatype primitive and therefore was removed from class list.",name));
			} else {			
				boolean isAbstract = false;
				if(name.contains("{abstract}")
				|| c.attr("isAbstract").equalsIgnoreCase("true")) {
					isAbstract = true;
					name = name.replace("{abstract}","").trim();
					logger.info(String.format("Class %s was classified as abstract.",name));
				}
				MetaClass umlClass = new MetaClass(name);
				umlClass.setId(c.attr("xmi:id"));
				umlClass.setAbstractClass(isAbstract);			
				umlClass = buildAttributes(umlClass,c);
				MetaPackage metaPackage = model.managePackageByName("");
				metaPackage.getClasses().add(umlClass);							
			}
		}
	}

	/**
	 * Extract interfaces from XML model and add it to meta model as classes.
	 * @param interfaces identified interfaces (XMI).
	 */
	private void buildInterfaces(Elements interfaces) {
		logger.trace("Called buildInterfaces(interfaces).");
		for(Element c : interfaces) {
			String name = c.attr("name");			
			MetaClass umlClass = new MetaClass(name);
			umlClass.setId(c.attr("xmi:id"));
			umlClass.setInterfaceClass(true);
			MetaPackage metaPackage = model.managePackageByName("");
			metaPackage.getClasses().add(umlClass);									
		}		
	}
	
	/**
	 * find data type by its relating {@code rangeId}, which is part of the XMI file.
	 * @param metaClass the meta class the attribute range belongs to.
	 * @param rangeId the internal data type id (provided by VParadigm XMI).
	 * @return data type name (e.g. string) or empty if void.
	 */
	private String findRangeType(MetaClass metaClass, String rangeId) {
		logger.trace(String.format("Called findRangeType(xmi-document,%s",rangeId));
		Elements e = xmi.select("packagedElement[xmi:type=uml:DataType]")
				   .select(String.format("[xmi:id=%s]",rangeId));
		
	
		if(e.first() == null) {
			e = xmi.select(String.format("[xmi:id=%s]",rangeId));
			if(e.first() != null) {
				 if(Util.isSupportedPrimitiveDatatype(e.first().attr("name"))) {
					 logger.warn(String.format("Did not find data type \"%s\" for class %s (range id %s). " +
								"However, attribute name has been detected as an primitive datatype, so the problem has been solved automatically by the converter. "
								,e.first().attr("name"),metaClass.getName(),rangeId));
				 } else {
					logger.warn(String.format("Did not find data type for class %s (range id %s). " +
							"The data range seems not to be a primitive data type, so the datatype was removed."
							,metaClass.getName(),rangeId,e.first().attr("name")));
					return "void";
				 }
			}
		}		
		return e.first().attr("name");
	}
	
	/**
	 * Add all attributes to meta class. Methods are ignored.
	 * @param umlClass a single meta class.
	 * @param xmiClass corresponding class from Visual Paradigm XMI file.
	 * @return meta class that contains all related attributes.
	 */
	private MetaClass buildAttributes(MetaClass umlClass, Element xmiClass) {
		logger.trace(String.format("Called buildAttributes(%s,jsoupXMIClass)",umlClass.getName()));
		Elements attrs = xmiClass.select("ownedAttribute");
		for (Element a : attrs) {
			if(!a.attr("association").isEmpty()) {
				logger.trace(String.format("Association %s found as attribute (skipped).",a.attr("association")));
				continue;
			}
			MetaAttribute metaAttribute = new MetaAttribute(a.attr("name"));
			metaAttribute.setVisibility(a.attr("visibility"));
			String rangeId = a.attr("type");
			String range = "void";
			if(!rangeId.isEmpty()) {
				logger.debug("Range id of " + umlClass + " is " + rangeId);
				range = findRangeType(umlClass,rangeId);
			}
			metaAttribute.setRange(range);
			umlClass.getAttributes().add(metaAttribute);
		}
		return umlClass;
	}
	
	/**
	 * Add associations between classes.
	 */
	private void addAssociations() {
		Elements associations = xmi.select("packagedElement[xmi:type=uml:Association]");
		for(Element ass : associations) {
			Elements elements = ass.select("ownedEnd");
			logger.debug("Found new association between classes.");
			String name = ass.attr("name"); 
			String id 	= ass.attr("xmi:id"); //ass.attr("memberEnd"); //if name exists
			logger.debug("id and name: " + id + "-" + name);
			if(elements.size() != 2) {
				logger.fatal(String.format("Wrong file format. Association %s contains more (or less) than 2 endpoints. Please check your XMI file or send us an e-mail.",name));
			} else {
				this.createAndLinkAssociation(id,elements,ass,name);
			}  
		}
	}	
	
	/**
	 * Creates new associations and links them to the meta classes.
	 * @param associationId the ID of the association (same for both link directions). 
	 * 		  Is used to identify the association globally (e.g. find comments). 
	 * @param elements a list of XMI elements containing end points (=XMI code).
	 * @param ass the association element (XMI) that contains the two endpoints and
	 * 		  navigation information.
	 * @param name name of the association if user named it, otherwise empty.
	 */
	private void createAndLinkAssociation(String associationId, Elements elements, Element ass, String name) {
		String lowerValueId   = elements.get(0).attr("xmi:id");
		String upperValueId   = elements.get(1).attr("xmi:id");
		String lowerValueFrom = elements.get(0).select("lowerValue").attr("value");
		String upperValueFrom = elements.get(0).select("upperValue").attr("value");
		String lowerValueTo   = elements.get(1).select("lowerValue").attr("value");
		String upperValueTo   = elements.get(1).select("upperValue").attr("value");
		
		String aggregationFrom  = elements.get(0).attr("aggregation").equals("shared") ? "true" : "false";
		String aggregationTo    = elements.get(1).attr("aggregation").equals("shared") ? "true" : "false";
		
		String compositionFrom  = elements.get(0).attr("aggregation").equals("composite") ? "true" : "false";
		String compositionTo    = elements.get(1).attr("aggregation").equals("composite") ? "true" : "false";
		
		MetaClass metaClassFrom = this.model.findClassById(elements.get(0).attr("type"));
		MetaClass metaClassTo   = this.model.findClassById(elements.get(1).attr("type"));				
		
		//break if class has not been foun
		if(metaClassFrom == null || metaClassTo == null) {
			logger.warn(String.format("One of the association endpoins contains an empty class so we skipped the association (association ID %s - name %s).",associationId,name));
			return;
		}
		
		boolean isBidirectional;
		String navigationElements = ass.attr("navigableOwnedEnd");
		String[] endpointIds = navigationElements.split(" "); 
		List<String> navigableEndpoints = new LinkedList<String>();
		navigableEndpoints.add(lowerValueId);
		navigableEndpoints.add(upperValueId);
		for(String epid : endpointIds) {
			navigableEndpoints.remove(epid); //remove navigable endpoints
		}
		if(navigableEndpoints.size() != 1) { //that means zero (best practice?) or two navigable endpoints exist.
			isBidirectional = true;
		} else if (navigableEndpoints.contains(lowerValueId)) { //id of endpoint still exists in the list - not navigable.
			isBidirectional = false;
		} else {
			isBidirectional = true;
		}
		
		
		logger.debug(String.format("association (%s) %s<->%s (%s;%s) found. Navigation is %s.",
						name.isEmpty() ? "noname" : name,
						metaClassFrom.getName(),metaClassTo.getName(),lowerValueFrom,upperValueFrom,
						isBidirectional ? "bidirectional" : "unidirectional"));
		
		MetaAssociation fromAssociation = new MetaAssociation(associationId, lowerValueId,name);			
		MetaAssociation toAssociation   = new MetaAssociation(associationId, upperValueId,name);
		
		fromAssociation.setFrom(new MetaClassPoint(metaClassFrom));
		fromAssociation.setTo(new MetaClassPoint(metaClassTo));
		fromAssociation.getFrom().setRangeSmart(lowerValueFrom, upperValueFrom);
		fromAssociation.getTo().setRangeSmart(lowerValueTo, upperValueTo);
		fromAssociation.setAggregation(Boolean.parseBoolean(aggregationFrom));
		fromAssociation.setComposition(Boolean.parseBoolean(compositionFrom));
		
		toAssociation.setTo(fromAssociation.getFrom());
		toAssociation.setFrom(fromAssociation.getTo());				
		toAssociation.setAggregation(Boolean.parseBoolean(aggregationTo));
		toAssociation.setComposition(Boolean.parseBoolean(compositionTo));
		
		if(isBidirectional) {
			fromAssociation.setInverseAssociation(toAssociation);
			toAssociation.setInverseAssociation(fromAssociation);
			metaClassFrom.getAssociations().add(fromAssociation);
			metaClassTo.getAssociations().add(toAssociation);
		} else {
			metaClassFrom.getAssociations().add(fromAssociation);
		}
	}
}
