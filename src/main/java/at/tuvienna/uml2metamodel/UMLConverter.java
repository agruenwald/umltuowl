package at.tuvienna.uml2metamodel;


import org.jsoup.nodes.Document;

import at.tuvienna.metamodel.MetaModel;

/**
 * @author Andreas Gruenwald
 * Convertion of UML code depending on specific tool and XMI syntax. Although XMI
 * should be standarized, most XMI codes are not transferable between 
 * different vendor's UML tools. Therefore the model nees to be transfered into a
 * stable meta model.
 */
public abstract class UMLConverter {
	/**
	 * Convertion of XMI syntax into a transferable meta model. Do not modify
	 * attribute names, etc. Keep it simple. Naming conventions are done afterwards
	 * via {@code harmonizer.Harmonizer} automatically (depending on presets). 
	 * @param umlDocument The XML/XMI document containing the UML model (vendor specific).
	 * @return transferable meta model that contains all UML elements that should be
	 * 		   converted into another model or code. 
	 */
	public abstract MetaModel convertModel(Document umlDocument);
}
