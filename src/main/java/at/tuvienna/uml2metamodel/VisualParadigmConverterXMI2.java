package at.tuvienna.uml2metamodel;

import java.sql.Array;
import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import at.tuvienna.metamodel.MetaAssociation;
import at.tuvienna.metamodel.MetaAttribute;
import at.tuvienna.metamodel.MetaClass;
import at.tuvienna.metamodel.MetaClassPoint;
import at.tuvienna.metamodel.MetaModel;
import at.tuvienna.metamodel.MetaPackage;

/**
 * @author Andreas Gruenwald
 * Converter for Visual Paradigm's XMI2.0 syntax.  
 */
public class VisualParadigmConverterXMI2 extends UMLConverter {
	private MetaModel model;
	private Document xmi;
	private static final Logger logger = Logger.getLogger(VisualParadigmConverterXMI2.class);
	
	private String cacheMainPackageName;

	/**
	 * Creation of a new Visual Paradigm XMI 2.0 metamodel (done automatically by factory).
	 */
	public VisualParadigmConverterXMI2() {
		this.model = new MetaModel("UML_VISUAL_PARADIGM_XMI_2_0_MODEL");
		this.cacheMainPackageName = "";
	}
	
	@Override
	public MetaModel convertModel(Document xmi) {
		logger.info(String.format("Convert model using %s XMI converter.",VisualParadigmConverterXMI2.class.getSimpleName()));
		this.xmi = xmi;
		this.model.setName(xmi.select("uml|Model").attr("name"));
		Elements classes = xmi.select("[xmi:type=uml:Class]");
		Elements interfaces = xmi.select("[xmi:type=uml:Interface]");
		Elements enumerations = xmi.select("[xmi:type=uml:Enumeration]");
		addDirtyEnumerations(enumerations);
		buildClasses(classes);
		buildInterfaces(interfaces);
		addGeneralization(classes);
		addGeneralization(interfaces);
		addAssociations();
		addComments();
		return model;
	}
	
	/**
	 * Bind UML comments to classes.
	 */
	private void addComments() {
		logger.trace("Called addComments(xmi,classes).");
		Elements comments = xmi.select("ownedComment");		
		for(Element c : comments) {
			String comment = c.attr("body");
			logger.trace(String.format("Found comment %s.",comment));
			String commentId = c.attr("xmi:id");
			Elements connections =  
				 xmi.select("packagedElement")
					.select("[xmi:type=GenericConnector]")
					.select("xmi|Extension");

			for(Element conn : connections) {
				String classId = "";
				if(conn.select("from").attr("idref").equals(commentId)) {
					classId = conn.select("to").attr("idref");
				} else if (conn.select("to").attr("idref").equals(commentId)) {
					classId = conn.select("from").attr("idref");
				}
				if(!classId.isEmpty()) {					
					MetaClass metaClass = model.findClassById(classId);
					if(metaClass == null) {
						//Comment for association
						List<MetaAssociation> assList = model.findAssociationById(classId);
						if(!assList.isEmpty()) {
							for(MetaAssociation ass : assList) {
								ass.getComment().add(comment);
								logger.trace(String.format("Added comment %s to %s.",comment,ass));
							}
						} else {
							logger.error(String.format("addComments(): Could not find class with id %s",classId));
						}
					} else {
						metaClass.getComment().add(comment);
						logger.trace(String.format("Added comment %s to %s.",comment,metaClass));
					}
				}
			}	
			
			String aId = c.attr("annotatedElement");
			if(!aId.isEmpty()) {
				List<String> l = new LinkedList<String>();
				if(aId.contains(" ")) {
					for (String i : aId.split(" ")) {
						l.add(i);
					}
				} else {
					l.add(aId);
				}
				for (String a : l) {
					MetaClass metaClass = this.model.findClassById(a);
					if(metaClass!=null) {
						metaClass.getComment().add(comment);
						logger.trace(String.format("Added comment %s to %s.",comment,metaClass));
					} else {
						logger.warn(String.format("Couldn't find annotated element for comment (annotationId=%s).",aId));
					}
				}
			}
		}
	}
	
	/**
	 * Add sub- and super class relations.
	 * @param classes identified classes (and interfaces).
	 */
	private void addGeneralization(Elements classes) {
		logger.trace("Called addGeneralization(classes).");
		for(Element c : classes) {
			Elements generalization = c.select("generalization");
			String classId 	    = c.attr("xmi:id");
			MetaClass clazz = model.findClassById(classId);
			logger.trace(String.format("Inspect class %s (generalization).",clazz));
			for(Element superclass : generalization) {				
				String superClassId = superclass.attr("general");
				MetaClass superClazz = model.findClassById(superClassId);
				if(superClazz == null) {
					logger.warn("========>"+superClassId+" (Interface?) ");
				} else {
					logger.debug(String.format("Class %s has superclass %s.",clazz,superClazz));
					superClazz.getSubclasses().add(clazz);
					clazz.getSuperclasses().add(superClazz);
				}
			}						
			
		}				
	}
	
	/**
	 * Add fine grained information (attributes) to meta classes.
	 * @param classes identified classes and interfaces.
	 */
	private void buildClasses(Elements classes) {
		logger.trace("Called buildClasses(classes).");
		for(Element c : classes) {
			c.select("ownedAttribute");
			String name = c.attr("name");			
			boolean isAbstract = c.attr("isAbstract").equals("true") ? true : false;
			if(name.contains("{abstract}")) {
				isAbstract = true;
				name = name.replace("{abstract}","").trim();
				logger.info(String.format("Class %s was classified as abstract.",name));
			}
			MetaClass umlClass = new MetaClass(name);
			umlClass.setId(c.attr("xmi:id"));
			umlClass.setAbstractClass(isAbstract);			
			umlClass = buildAttributes(umlClass,c);
			MetaPackage metaPackage = model.managePackageByName(this.determinePackageName(c,umlClass));
			metaPackage.getClasses().add(umlClass);							
		}
	}
	

	/**
	 * Extract interfaces from XML model and add it to meta model as classes.
	 * @param interfaces identified interfaces (XMI).
	 */
	private void buildInterfaces(Elements interfaces) {
		logger.trace("Called buildInterfaces(interfaces).");
		for(Element c : interfaces) {
			String name = c.attr("name");			
			MetaClass umlClass = new MetaClass(name);
			umlClass.setId(c.attr("xmi:id"));
			umlClass.setInterfaceClass(true);
			MetaPackage metaPackage = model.managePackageByName(this.determinePackageName(c,umlClass));
			metaPackage.getClasses().add(umlClass);									
		}		
	}
	
	/**
	 * find data type by its relating {@code rangeId}, which is part of the XMI file.
	 * @param metaClass the meta class the attribute range belongs to.
	 * @param rangeId the internal data type id (provided by VParadigm XMI).
	 * @return data type name (e.g. string) or empty if void.
	 */
	private String findRangeType(MetaClass metaClass, String rangeId) {
		logger.trace(String.format("Called findRangeType(xmi-document,%s",rangeId));
		Elements e = xmi.select("packagedElement[xmi:type=uml:DataType]")
				   .select(String.format("[xmi:id=%s]",rangeId));
		
		/* NOT IN USE  
		if(e.first() == null) {
			e = xmi.select(String.format("[xmi:id=%s]",rangeId));
			if(e.first() != null) {
				logger.warn(String.format("Did not find data type for class %s (range id %s). " +
						"Try to use one of the classes, but note that only primitive data types are " +
						"supported for attribute values."
						,metaClass.getName(),rangeId));
			}
		}
		*/
		if(e.isEmpty()) {
			return "";
		} else {
			return e.first().attr("name");
		}
	}
	
	/**
	 * Add all attributes to meta class. Methods are ignored.
	 * @param umlClass a single meta class.
	 * @param xmiClass corresponding class from Visual Paradigm XMI file.
	 * @return meta class that contains all related attributes.
	 */
	private MetaClass buildAttributes(MetaClass umlClass, Element xmiClass) {
		logger.trace(String.format("Called buildAttributes(%s,jsoupXMIClass)",umlClass.getName()));
		Elements attrs = xmiClass.select("ownedAttribute");
		for (Element a : attrs) {
			if(!a.attr("association").isEmpty()) {
				logger.trace(String.format("Association %s found as attribute (skipped).",a.attr("association")));
				continue;
			}
			MetaAttribute metaAttribute = new MetaAttribute(a.attr("name"));
			metaAttribute.setVisibility(a.attr("visibility"));
			String rangeId = a.attr("type");
			String range = "void";
			if(!rangeId.isEmpty()) {
				logger.debug("Range id of " + umlClass + " is " + rangeId);
				range = findRangeType(umlClass,rangeId);
				//allow to connect classes!
				if(range.isEmpty()) {
					MetaClass linkedClass = this.model.findClassById(rangeId);
					if(linkedClass==null) {
						logger.warn(String.format("There seems to be something wrong regarding the linkage of an attribute: %s.",metaAttribute));
					} else {
						MetaAssociation attribute = new MetaAssociation("____"+umlClass+"-"+metaAttribute, "____"+umlClass+"-"+metaAttribute, metaAttribute.getName());		
						attribute.setFrom(new MetaClassPoint(umlClass));
						attribute.setTo(new MetaClassPoint(linkedClass));
						attribute.getTo().setRangeSmart("1", "1");
						attribute.getFrom().setRangeSmart("0", "*"); 
						umlClass.getAssociations().add(attribute);
						continue;
					}
				}
			}
			metaAttribute.setRange(range);
			umlClass.getAttributes().add(metaAttribute);
		}
		return umlClass;
	}
	
	/**
	 * Add associations between classes.
	 */
	private void addAssociations() {
		logger.trace("Called addAssociations()");
		Elements associations = xmi.select("packagedElement[xmi:type=uml:Association]");
		for(Element ass : associations) {
			Elements elements = ass.select("ownedEnd");
			logger.debug("Found new association between classes.");
			String name = ass.attr("name"); 
			String id 	= ass.attr("xmi:id"); //ass.attr("memberEnd"); //if name exists
			logger.debug("id and name: " + id + "-" + name);
			logger.debug(elements.size());
			if(elements.size() > 2) {
				logger.fatal(String.format("Wrong file format. Association %s contains more than 2 endpoints. Please check your XMI file or send us an e-mail.",name));
			} else if (elements.size() == 2) {
				//Visual Paradigm 8.*
				if(elements.get(0).attr("isnavigable").equalsIgnoreCase("false")) {
					this.createAndLinkAssociation(id, elements,name, true, false);	
				} else if (elements.get(1).attr("isnavigable").equalsIgnoreCase("false")) {
					this.createAndLinkAssociation(id, elements,name, false, true);
				} else {
					createAndLinkAssociation(id, elements,name, true, true);
				}
				
			} 
			//if link goes only in one direction
			else if(elements.size() == 1) {
				Elements ownedAttributes = xmi.select("ownedAttribute[association="+elements.get(0).attr("association")+"]");				
				if(ownedAttributes.size() == 1) {
					elements.addAll(ownedAttributes);
					this.createAndLinkAssociation(id, elements, name, true, false);
				} else {
					logger.error(String.format("Invalid XMI structure: There was XML code that was not expected regarding unidirectional link with name \"%s\" and id %s",name,id));
				}
			}
		}
	}
	
	/**
	 * Enumerations are transformed into an abstract class, where each enumeration value will be represented by a subclass in the metamodel.
	 * There are other possible solutions for OWL, however this is the simpliest one.
	 * @param enumerations a list of xml elements of the enumeration classes.
	 */
	private void addDirtyEnumerations(Elements classes) {
		logger.trace("Called addDirtyEnumerations(classes).");
		for(Element c : classes) {
			c.select("ownedAttribute");
			String name = c.attr("name");			
			MetaClass umlClass = new MetaClass(name);
			umlClass.setEnumClass(true);
			umlClass.setId(c.attr("xmi:id"));
			umlClass.setAbstractClass(true);			
			//umlClass = buildAttributes(umlClass,c);
			MetaPackage metaPackage = model.managePackageByName(this.determinePackageName(c,umlClass));
			metaPackage.getClasses().add(umlClass);
			
			Elements literals = c.getElementsByTag("ownedLiteral");
			for(Element l : literals) {
				if(l.attr("name").isEmpty()) {
					logger.warn(String.format("Empty literal in enumeration class %s.",umlClass.getName()));
				} else {
					MetaClass eClass = new MetaClass(l.attr("name"));
					eClass.setId(c.attr("xmi:id")+"-"+eClass.getName());
					eClass.setAbstractClass(false);
					eClass.setEnumClass(true);
					metaPackage.getClasses().add(eClass);	
					eClass.getSuperclasses().add(umlClass);
					umlClass.getSubclasses().add(eClass);	
				}	
			}						
		}
	}
	
	/**
	 * Determine the package name for a specified UML class.
	 * Problems occur when determining package name because depending on
  	 * the package that is opened in Visual Paradigm, file structure
	 * is different (opened package is not surrounded by packet element). 
	 * @param c corresponding class or interface XMI element.
	 * @param umlClass class or interface that should be proofed.
	 * @return name of package.
	 */
	private String determinePackageName(Element c, MetaClass umlClass) {	
		String packageName = "";			
		if (c.parent().tag().getName().equalsIgnoreCase("packagedElement") 
		&& c.parent().attr("xmi:type").equalsIgnoreCase("uml:Package")) {
			packageName = c.parent().attr("name");
		} else if (c.parent().tag().getName().equalsIgnoreCase("uml:Model")) {
			if(this.cacheMainPackageName.isEmpty()) {
				Elements diagrams = c.parent().select("uml|Diagram").select("[diagramType=ClassDiagram]");
				for(Element d : diagrams) {
					if(d.parent().tag().getName().equalsIgnoreCase("packagedElement") 
					&& d.parent().attr("xmi:type").equalsIgnoreCase("uml:Package")) {
						continue;
					}
					
					Elements collapsedPackages = xmi.select("packagedElement[xmi:type=uml:Package]")
					  .select("[name="+d.attr("name")+"]");
					  
					if(!collapsedPackages.isEmpty()) {
						continue;
					}
					packageName = d.attr("name");
					this.cacheMainPackageName = packageName;
					
					
				}
				logger.debug(String.format("Package %s was opened during Visual Paradigm export - %s is an element of it.",packageName,c));
			} else {
				packageName = this.cacheMainPackageName;
			}
		} else {
			packageName = this.cacheMainPackageName;
			logger.debug(String.format("Cannot name of package for element %s. This is an error if there are more than a single packages.",c));
		}			
		logger.trace("parent of " + umlClass.getName() + ":"+packageName);
		return packageName;
	}
	
	/**
	 * Creates new associations and links them to the meta classes.
	 * @param associationId the ID of the association (same for both link directions). 
	 * 		  Is used to identify the association globally (e.g. find comments). 
	 * @param elements a list of XMI elements containing end points (=XMI code).
	 * @param name name of the association if user named it, otherwise empty.
	 * @param isBidirectionalFrom navigation allowed from --> to.
	 * @param isBidirectionalTo navigation allowed to --> from.
	 */
	private void createAndLinkAssociation(String associationId, Elements elements, String name, 
			boolean isBidirectionalFrom,
			boolean isBidirectionalTo) {
		String lowerValueId   = elements.get(0).attr("xmi:id");
		String upperValueId   = elements.get(1).attr("xmi:id");
		String lowerValueFrom = elements.get(0).select("lowerValue").attr("value");
		String upperValueFrom = elements.get(0).select("upperValue").attr("value");
		String lowerValueTo   = elements.get(1).select("lowerValue").attr("value");
		String upperValueTo   = elements.get(1).select("upperValue").attr("value");
		
		String aggregationFrom  = elements.get(1).attr("aggregation").equals("shared") ? "true" : "false";
		String aggregationTo    = elements.get(0).attr("aggregation").equals("shared") ? "true" : "false";
		
		String compositionFrom  = elements.get(1).attr("aggregation").equals("composite") ? "true" : "false";
		String compositionTo    = elements.get(0).attr("aggregation").equals("composite") ? "true" : "false";
		
		MetaClass metaClassFrom = this.model.findClassById(elements.get(0).attr("type"));
		MetaClass metaClassTo   = this.model.findClassById(elements.get(1).attr("type"));				
		
		logger.debug(String.format("association (%s) %s<->%s (%s;%s) found. Navigation is %s.",
						name.isEmpty() ? "noname" : name,
						metaClassFrom.getName(),metaClassTo.getName(),lowerValueFrom,upperValueFrom,
						isBidirectionalFrom && isBidirectionalTo ? "bidirectional" : "unidirectional"));
		
		MetaAssociation fromAssociation = new MetaAssociation(associationId, lowerValueId,name);			
		MetaAssociation toAssociation   = new MetaAssociation(associationId, upperValueId,name);
		
		fromAssociation.setFrom(new MetaClassPoint(metaClassFrom));
		fromAssociation.setTo(new MetaClassPoint(metaClassTo));
		fromAssociation.getFrom().setRangeSmart(lowerValueFrom, upperValueFrom);
		fromAssociation.getTo().setRangeSmart(lowerValueTo, upperValueTo);
		fromAssociation.setAggregation(Boolean.parseBoolean(aggregationFrom));
		fromAssociation.setComposition(Boolean.parseBoolean(compositionFrom));
		
		toAssociation.setTo(fromAssociation.getFrom());
		toAssociation.setFrom(fromAssociation.getTo());				
		toAssociation.setAggregation(Boolean.parseBoolean(aggregationTo));
		toAssociation.setComposition(Boolean.parseBoolean(compositionTo));
		
		if(isBidirectionalFrom && isBidirectionalTo) {
			fromAssociation.setInverseAssociation(toAssociation);
			toAssociation.setInverseAssociation(fromAssociation);
			metaClassFrom.getAssociations().add(fromAssociation);
			metaClassTo.getAssociations().add(toAssociation);
		} else if (isBidirectionalFrom) {
			metaClassFrom.getAssociations().add(fromAssociation);
		} else if (isBidirectionalTo) {
			metaClassTo.getAssociations().add(toAssociation);
		} else {
			logger.warn(String.format("No navigation between %s and %s found???",metaClassFrom.getName(),metaClassTo.getName()));
		}
	}
}
