package at.tuvienna.main;


import java.io.IOException;
import java.util.Iterator;

import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.semanticweb.owlapi.model.OWLException;

import at.tuvienna.harmonizer.Harmonizer;
import at.tuvienna.metamodel.MetaModel;
import at.tuvienna.metamodel.MetaPackage;
import at.tuvienna.metamodeltoowl.OWLMaker;
import at.tuvienna.uml2metamodel.ConverterNotExistsException;
import at.tuvienna.uml2metamodel.TransferFactory;
import at.tuvienna.uml2metamodel.UMLConverter;
import at.tuvienna.utils.SettingsReader;
import at.tuvienna.utils.Util;

/**
 * 
 * @author Andreas Gruenwald <a.gruenw@gmail.com>
 * Main class for OWL transformation. Requires an input file 
 * (Visual Paradigm XMI 2.0) and transforms it into an output file (OWL).
 */
public class Main {
	private final static Logger logger = Logger.getLogger(Main.class);
	public final static String VERSION = "2.0";
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			//check input parameters and create meta model.
			MetaModel metaModel = loadMetaModel(args);
			if (metaModel == null) {
				return;
			}
					
			//Harmonize, e.g. create IRI's.
			metaModel = harmonizeModel(metaModel);		
			
			//Create OWL Ontology
			if (createOWL(metaModel)) {
				logger.info(String.format("Finished transformation process (%s class elements contained in resulting ontology).",metaModel.getSize()));
				for(MetaPackage pkg : metaModel.getPackages()) {
					logger.info(String.format (" *Package %s:",pkg.getName()));
					logger.info(String.format("  ==> %s classes",pkg.getClasses().size()));
					logger.info(String.format("  ==> %s associations",pkg.getSizeAssociations()));
					logger.info(String.format("  ==> %s attributes",pkg.getSizeAttributes()));	
				}
			}
		} catch (Exception e) {
			logger.fatal(String.format("Transformation process failed. " +
							"Please make shure that you used the correct input format (e.g. Visual Paradigm XMI 2.1 export files and not the vpp - project files)." +
							"Exception: %s. If you have further problems, send a mail to <a.gruenw@gmail.com> and describe/attach your input file.",
					e.getMessage()));
			 e.printStackTrace();
			return;
		}
	}
	
	/**
	 * Validate input parameters and load meta model.
	 * @param args the input arguments (e.g. from console by user).
	 * @return created meta model or {@code null}, if parameters are missing or
	 * 		   transformation failed. Errors are logged automatically.
	 */
	public static MetaModel loadMetaModel(String[] args) {
		try {
			SettingsReader.loadSettings(); //load settings
		} catch (IOException e) {
			logger.fatal(String.format("Failed to load default settings."));
			return null;
		}
		
		//Handle input parameters and settings
		if (new ParameterHandling().validateParameters(args)) {
			return null;
		}
		
		String filename 		= SettingsReader.readProperty(ParameterHandling.INPUT);
		String converterName 	= SettingsReader.readProperty(ParameterHandling.CONVERTER);
		String iriName 			= SettingsReader.readProperty(ParameterHandling.IRI);
			
		Document umlDocument = null;
		try {			
			String charset = SettingsReader.readProperty("encoding").isEmpty() ? TransferFactory.getEncoding(converterName) 
						   : SettingsReader.readProperty("encoding");
			logger.info(String.format("Try to read %s using %s charset.",filename, charset));
			String fileContent = XMLPreprocessor.readPreprocessedFileContent(filename, charset);
			logger.info(String.format("Preprocessed %s. Parse XML...",filename));
			umlDocument = Jsoup.parse(fileContent);
		} catch (IOException e) {
			logger.fatal(String.format("Cannot not open input file %s.",filename));
			return null;
		}
		
		logger.info(String.format("Looking for converter %s...",converterName));
		UMLConverter converter = null;
		try {
			converter = TransferFactory.createConverter(converterName);
		} catch (ConverterNotExistsException e) {
			logger.fatal(String.format("Converter %s does not exist. Please specify an existing one.",converterName));
			return null;
		}
		
		logger.info(String.format("Convert XML into meta model..."));
		MetaModel metaModel = converter.convertModel(umlDocument);
		if(SettingsReader.readProperty(ParameterHandling.VERBOSE).equalsIgnoreCase("true")) {
			//logger.info(String.format("Built metamodel: \n %s",metaModel));	
		} else {
			logger.debug(String.format("Built metamodel: \n %s",metaModel));
		}
		logger.info(String.format("Convert metamodel into OWL using OWL API (IRI name is %s)...",iriName));
		
		return metaModel;
	}
	
	/**
	 * Harmonize the meta model, e.g. create IRI's and prefix associations.
	 * @param metaModel the meta model.
	 * @return harmonized meta model.
	 */
	public static MetaModel harmonizeModel(MetaModel metaModel) {
		Harmonizer harmonizer = new Harmonizer();
		MetaModel metaModelNew = harmonizer.harmonize(metaModel);
		if(SettingsReader.readProperty(ParameterHandling.VERBOSE).equalsIgnoreCase("true")) {
			logger.info(String.format("Built metamodel (after harmonizing): \n %s",metaModelNew));
		}
		return metaModelNew;
	}
	
	/**
	 * Create OWL meta model(s) and output it to file(s). If errors occur,
	 * they will be logged.
	 * @param metaModel the meta model.
	 * @return {@code true}, if successfully created OWL file(s).
	 */
	public static boolean createOWL(MetaModel metaModel) {
		String outputName  		= SettingsReader.readProperty(ParameterHandling.OUTPUT);
		String iriName 			= SettingsReader.readProperty(ParameterHandling.IRI);
		Iterator<MetaPackage> it = metaModel.getPackages().iterator();
		try {
			if(metaModel.getPackages().size() > 1) {
				int i = 1;
				while(it.hasNext()) {					
					MetaPackage metaPackage = it.next();
					String append = "";
					if (metaPackage.getName().isEmpty()) {
						append = String.valueOf(i);
						i++;
					} else {
						append = Util.metaNameConvertion(metaPackage.getName(),"","",true);
					}
					
					int splitIndex = outputName.lastIndexOf(".");
					String numberedOutputName = outputName.substring(0,splitIndex) 
											  + "_" + append
											  + outputName.substring(splitIndex,outputName.length());
					
					OWLMaker.toOwl(numberedOutputName, iriName, metaPackage);
					logger.info(String.format("Created file %s.",numberedOutputName));					
				}
			} else {
				while(it.hasNext()) {
					MetaPackage metaPackage = it.next();
					OWLMaker.toOwl(outputName, iriName, metaPackage);
					logger.info(String.format("Created file %s.",outputName));
				}
			}
		} catch (OWLException e) {
			System.err.println(e.getMessage());
			logger.fatal(String.format("Failed building OWL ontology into file %s. Details: %s.",outputName,e.getMessage()));
			return false;
		}
		return true;
	}
}
