package at.tuvienna.main;

/**
 * 
 * @author Andreas Gruenwald <a.gruenw@gmail.com>
 * Specification of an input parameter (internal)
 *
 */
public class Parameter {
	private final String name;
	private String description;
	private final boolean isMandatory;
	
	/**
	 * Creation of a new input parameter.
	 * @param name the name of the parameter as specified by the user. 
	 * @param description the description for the user.
	 * @param isMandatory if {@code true}, the parameter is mandatory.
	 */
	public Parameter(String name, String description, boolean isMandatory) {
		this.name = name;
		this.description = description;
		this.isMandatory = isMandatory;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return name;
	}

	public boolean isMandatory() {
		return isMandatory;
	}
	
	public String toString() {
		return name;
	}
	
	public String toConsole() {
		String res = String.format("%s: %s%s",name,description, isMandatory ? " (mandatory)" : "(optional)");
		return res;
	}
}
