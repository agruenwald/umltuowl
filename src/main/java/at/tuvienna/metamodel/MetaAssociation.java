package at.tuvienna.metamodel;




/**
 * @author Andreas Gruenwald <a.gruenw@gmail.com>
 * Represents an association between classes. In OWL, an 
 * association is represented by an object property.
 */
public class MetaAssociation extends MetaElement {
	private String id;
	private String associationId;
	private String name;
	private MetaClassPoint from;
	private MetaClassPoint to;
	private boolean isComposition;
	private boolean isAggregation;
	
	private MetaAssociation inverseAssociation;
	
	//private final static Logger logger = Logger.getLogger(MetaAssociation.class);

	/**
	 * Create a new association (unidirectional), composition or aggregation.
	 * @param id internal id can be used for parsing XMI - unique for  each unidirectional relation.
	 * @param associationId id of association. if two associations relate to each other
	 * 		  (e.g. bidirectional associations) both association have an unique id, but the 
	 * 		  same association id. Is used primarily for parsing purposes, e.g. linking comments,
	 * 		  so can be used for internal purposes and does not influence OWL creation.
	 * @param name the name of the association if empty or otherwise blank.
	 */
	public MetaAssociation(String associationId, String id, String name) {
		super();
		this.from=null;
		this.to=null;
		this.name=name;
		this.id=id;		
		this.associationId = associationId;
		this.inverseAssociation=null;
		this.isComposition=false;
		this.isAggregation=false;
	}
	
	/**
	 * Creates a new meta association without name. This constructor must
	 * be avoided and should only be used for automated processing,
	 * such as serialization.
	 */
	public MetaAssociation() {
		this("","","");
	}
	
	public String getAssociationId() {
		return associationId;
	}


	public String getId() {
		return id;
	}

	/**
	 * Only use for automated processing!
	 * @param id
	 */
	public void setAssociationId(String id) {
		this.associationId=id;
	}

	/**
	 * Only use for automated processing!
	 * @param id
	 */
	public void setId(String id) {
		this.id=id;
	}
	
	public void setName(String name) {
		this.name = name;
	}


	public String getName() {
		return name;
	}

	public MetaClassPoint getFrom() {
		return from;
	}

	public void setFrom(MetaClassPoint from) {
		this.from = from;
	}

	public MetaClassPoint getTo() {
		return to;
	}

	public void setTo(MetaClassPoint to) {
		this.to = to;
	}

	public boolean isComposition() {
		return isComposition;
	}

	public void setComposition(boolean isComposition) {
		this.isComposition = isComposition;
	}

	public boolean isAggregation() {
		return isAggregation;
	}

	public void setAggregation(boolean isAggregation) {
		this.isAggregation = isAggregation;
	}

	public MetaAssociation getInverseAssociation() {
		return inverseAssociation;
	}

	public void setInverseAssociation(MetaAssociation inverseAssociation) {
		this.inverseAssociation = inverseAssociation;
	}
	
	
	public String toString() {
		String s;
		s = String.format(" %s%s%s-->%s (%s%s%s).",
				from.getMetaClass()!= null ? from.getMetaClass().getName() : "----ERROR----UNKNOWN---FROM.getMetaClass----",
				this.inverseAssociation == null ? "---" : "<--",
				name.isEmpty() ? "noname" : name,				
				to.getMetaClass() != null  ? to.getMetaClass().getName() : "----ERROR----UNKNOWN---TO.getMetaClass----",
				to.getRange().toString(),//  ? from.getMetaClass().getName() : "----ERROR----UNKNOWN---FROM.getMetaClass----",
				this.isAggregation ? " - aggregation" : "",
				this.isComposition ? " - composition" : ""				
			);
		
		for(String c : this.getComment()) {
			s += String.format("\n       Comment: %s", c);
		}
		return s;
	}
	
}
