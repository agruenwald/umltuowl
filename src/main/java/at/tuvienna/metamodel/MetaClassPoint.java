package at.tuvienna.metamodel;

import java.util.List;

import org.apache.log4j.Logger;

import at.tuvienna.utils.TransformationException;
import at.tuvienna.utils.Util;

/**
 * 
 * @author andreas_gruenwald
 * ClassPoints must only be used within meta associations. They are actually
 * inner classes, but have been moved into a separate class to enable better
 * automated serialization mechanisms, such as for JSON.
 */
public class MetaClassPoint {
	private final static Logger logger = Logger.getLogger(MetaClassPoint.class);
	
	private MetaClass metaClass;
	private String metaClassId; //for serialization purposes
	private List<String> range;
	
	/**
	 * Strictly only use in automated processings, such
	 * as serialization!
	 */
	public MetaClassPoint() {
		this(null);
	}
	
	public MetaClassPoint(MetaClass metaClass) {
		this.metaClass = metaClass;
		if(metaClass==null) {
			this.metaClassId="";
		} else {
			this.metaClassId=metaClass.getId();
		}
	}
	
	/**
	 * Sets the range of an association.
	 * @param min an single value or blank or "*"
	 * or a vector of elements separated by colon, e.g. 1,2,3,4. 
	 * @param max same as parameter min or end of the range, e.g. 10 or * or even blank.
	 * Usually higher than min.
	 */
	public void setRangeSmart(String min, String max) {
		this.setRangeSmart(min, max, null);
	}
	
	public void setRangeSmart(String min, String max, MetaAssociation metaAssociation) {
		try {
			this.range = Util.normalizeRange(min,max);
		} catch (TransformationException e) {
			if(metaAssociation!=null) {
				logger.error(String.format("Could not transform range for association of class %s: %s",metaAssociation.getFrom(),e.getMessage()));
			} else {
				logger.error(String.format("Could not transform range for association of endpoint %s: %s",this.getClass(),e.getMessage()));
			}
		}
	}
	
	/**
	 * @return a sorted list of all range values where blank is lowest and "*" is highest.
	 * All other values (in between) are numeric. List contains at least one element if filled
	 * using {@code setRange()}.
	 */
	public List<String> getRange() {
		return range;
	}
	
	/** Strictly only use in automated processing mechanisms, 
	 * such as serialization! 
	**/
	public void setRange(List<String> range) {
		this.range=range;
	}
	
	/**
	 * @return name of the metaclass (=range) the class point belongs to.
	 */
	public MetaClass getMetaClass() {
		if(this.metaClass==null && !this.metaClassId.isEmpty()) {
			//TODO problem with serialization/JSON
		}
		return metaClass;
	}	
	
	/** Necessary since automation is taking place
	**/
	public void setMetaClass(MetaClass metaClass) {
		this.metaClassId=metaClass.getId();
		this.metaClass=metaClass;
	}
	
	/**
	 * @return is used when serializing the files (flat)
	 */
	public String getMetaClassId() {
		return this.metaClassId;
	}
	
	/**
	 * is only used by automated processing mechanisms to flatten
	 * the serialization process.
	 * @param metaClassId the metaClass id.
	 */
	public void setMetaClassId(String metaClassId) {
		this.metaClassId=metaClassId;
	}
}
