package at.tuvienna.metamodel;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
/**
 * @author Andreas Gruenwald <a.gruenw@gmail.com>
 * Represents a single UML package an is transformed into a single ontology.
 * Packages are defined differently in UML tools: In Visual Paradigm a package
 * usually is presented in a new class diagram. Therefore, one has to differ
 * between default UML packages and UML tool constructs. Packages therefore not
 * always might be supported. 
 */
public class MetaPackage extends MetaElement {
	private final String name;
	private Set<MetaClass> classes;
	
	/**
	 * Create the package. Usually, each package is transformed into a single OWL
	 * Ontology, but perhaps they also may be merged into a single one.
	 * @param name the package name - must be unique. case sensitive.  
	 */
	public MetaPackage(String name) {
		super();
		this.classes=new HashSet<MetaClass>();
		this.name = name;
	}
	
	/**
	 * Creates a new meta package without name. This constructor must
	 * be avoided and should only be used for automated processing,
	 * such as serialization.
	 */
	public MetaPackage() {
		this("");
	}
	
	/**
	 * @return returns a set of all classes in the package, including
	 * interfaces and abstract classes.
	 */
	public Set<MetaClass> getClasses() {
		return classes;
	}

	/**
	 * @param classes set meta model classes or add one/multiple classes/
	 * interfaces/abstract classes to the meta package.
	 */
	public void setClasses(Set<MetaClass> classes) {
		this.classes = classes;
	}
	
	/**
	 * find a meta class by its identifier (internal) an return it.
	 * @param classId unique class id (!= class name).
	 * @return the meta class with the given class id or {@code null},
	 * if no meta class matched.
	 */
	public MetaClass findClassById(String classId) {
		Iterator<MetaClass> it = this.classes.iterator();
		while(it.hasNext()) {
			MetaClass c = it.next();
			if(c.getId().equals(classId)) {
				return c;
			}
		}
		return null;
	}
	
	/**
	 * find a meta class by its name and return it. 
	 * @param className unique class name
	 * @return the meta class with the given class name or {@code null},
	 * if no meta class matched.
	 */
	public MetaClass findClassByName(String className) {
		Iterator<MetaClass> it = this.classes.iterator();
		while(it.hasNext()) {
			MetaClass c = it.next();
			if(c.getName().equals(className)) {
				return c;
			}
		}
		return null;
	}

	/**
	 * find a meta class by its name and return it. In some cases it can
	 * be, that a class names occurs multiple time, e.g. in ArgoUML all
	 * classes (from different drawings) are merged into a single XMI file.
	 * @param className unique class name
	 * @return a list of the meta classes found  with the given class name or an empty
	 * list, if if no meta classes matched.
	 */
	public List<MetaClass> findMultipleClassesByName(String className) {
		List<MetaClass> result = new LinkedList<MetaClass>();
		Iterator<MetaClass> it = this.classes.iterator();
		while(it.hasNext()) {
			MetaClass c = it.next();
			if(c.getName().equals(className)) {
				result.add(c);
			}
		}
		return result;
	}
	
	/**
	 * @return return a list of all classes sorted (interfaces,abstracts,concrete classes).
	 */
	public List<MetaClass> findAllClassesSortedByAbstract() {
		List<MetaClass> interfaces = new LinkedList<MetaClass>();
		List<MetaClass> abstracts = new LinkedList<MetaClass>();
		List<MetaClass> concretes = new LinkedList<MetaClass>();
		
		for(MetaClass metaClass : this.classes) {
			if(metaClass.isAbstractClass()) {
				abstracts.add(metaClass);
			} else if (metaClass.isInterfaceClass()) {
				interfaces.add(metaClass);
			} else {
				concretes.add(metaClass);
			}
		}
		List<MetaClass> list = new LinkedList<MetaClass>();
		list.addAll(interfaces);
		list.addAll(abstracts);
		list.addAll(concretes);
		return list;
	}
	
	public int getSizeAssociations() {
		int size = 0;
		int bi = 0;
		int uni = 0;
		Iterator<MetaClass> it = this.getClasses().iterator();
		while(it.hasNext()) {
			MetaClass c = it.next();
			for(MetaAssociation a : c.getAssociations()) {
				if(a.getInverseAssociation()==null) {
					bi++;
				} else {
					uni++;
				}
			}
		}
		size = bi + uni/2;
		return size;		
	}
	
	public int getSizeAttributes() {
		int size = 0;
		Iterator<MetaClass> it = this.getClasses().iterator();
		while(it.hasNext()) {
			MetaClass c = it.next();
			size += c.getAttributes().size();
		}
		return size;
	}
	
	public int getSizeGeneralizations() {
		Iterator<MetaClass> it = this.getClasses().iterator();
		int size = 0;
		while(it.hasNext()) {
			MetaClass c = it.next();
			size += c.getSuperclasses().size();
		}
		return size;
	}
	
	/**	
	 * @return package name.
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return whole model is returned as a string containing package, class and 
	 * attribute information.
	 */
	public String toString() {
		String s = "";
		Iterator<MetaClass> it = this.classes.iterator();
		while(it.hasNext()) {
			MetaClass clazz = it.next();
			
			String attrs = "";
			if (clazz.isAbstractClass() || clazz.isInterfaceClass()) {
				attrs = clazz.isAbstractClass() ? "abstract" : "";
				if(!attrs.isEmpty()) {
					attrs = attrs + (clazz.isInterfaceClass() ? ",interface" : "");
				} else {
					attrs = attrs + (clazz.isInterfaceClass() ? "interface" : "");
				}
				attrs = " (" + attrs + ")";
			}
			
			s += "Class " + clazz + attrs + "\n";
			Iterator<MetaClass> itSubclasses = clazz.getSubclasses().iterator();
			while (itSubclasses.hasNext()) {
				MetaClass subclass = itSubclasses.next();
				s+= "==> subclass " + subclass + "\n";
			}
			Iterator<MetaClass> itSuperclasses = clazz.getSuperclasses().iterator();
			while (itSuperclasses.hasNext()) {
				MetaClass superclass = itSuperclasses.next();
				s+= "==> superclass " + superclass + "\n";
			}
			
			Iterator<String> itComments = clazz.getComment().iterator();
			while(itComments.hasNext()) {
				String c = itComments.next();
				s+= "         Comment: " + c + "\n";
			}
			
			Iterator<MetaAttribute> itAttributes = clazz.getAttributes().iterator();
			while(itAttributes.hasNext()) {
				MetaAttribute attr = itAttributes.next();
				s += "   " + attr;
			}			
			
			Iterator<MetaAssociation> itAssociation = clazz.getAssociations().iterator();
			while(itAssociation.hasNext()) {
				MetaAssociation ass = itAssociation.next();
				s += "  + " + ass + "\n";
			}
		}
		return s;
	}
}
