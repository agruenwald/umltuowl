package at.tuvienna.metamodel;


/**
 * @author Andreas Gruenwald <a.gruenw@gmail.com>
 * Represents an attribute of a class. An attribute consists of 
 * a name, a datatype (=range; even, if range might be "UNKNOWN"),
 * a visibility status, and some optional information.
 */
public class MetaAttribute extends MetaElement {
	public final static String VOID 	    = "void";
	public final static String DATETIME	    = "datetime";
	public final static String BOOLEAN 	    = "boolean";	
	public final static String TIMESTAMP    = "timestamp";
	public final static String TIME  	    = "time";
	
	public final static String  PUBLIC 		= "public";
	public final static String  PROTECTED 	= "protected";
	public final static String  PRIVATE 	= "private";
	public final static String  PACKAGE 	= "package";
	
	private String name;
	private String visibility;
	private String range;

	public MetaAttribute(String name) {
		super();
		this.name=name;
		this.visibility = PUBLIC;		
		this.range = VOID;
	}
	
	
	/**
	 * Creates a new meta attribute without name. This constructor must
	 * be avoided and should only be used for automated processing,
	 * such as serialization.
	 */
	public MetaAttribute() {
		this("");
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return visibility of attribute (public, private or protected)
	 */
	public String getVisibility() {
		return visibility;
	}
	
	/**
	 * set visiblity of attribute.
	 * @param visibility public, private or protected are allowed.
	 */
	public void setVisibility(String visibility) {
		this.visibility = visibility;
	}

	/**
	 * @return range of attribute. Values see {@code setRange(String range)}. 
	 */
	public String getRange() {
		return range;
	}

	/**
	 * Set range of attribute.
	 * @param range of attribute. Allowed are 
	 */
	public void setRange(String range) {
		if(range.equalsIgnoreCase("bool") 
		|| range.equalsIgnoreCase("boolean")) {
			range = BOOLEAN;
		}
		this.range = range;
	}
	
	/**
	 * @return attribute in UML notation is returned (with leading sign for visibility).
	 */
	public String toString() {
		String v="?";
		if(visibility.equals(PRIVATE)) {
			v="-";
		} else if (visibility.equals(PUBLIC)) {
			v="+";
		} else if (visibility.equals(PROTECTED)) {
			v="#";
		} else if (visibility.equals(PACKAGE)) {
			v="~";
		}
		return v+name+":"+getRange()+"\n";
	}
}