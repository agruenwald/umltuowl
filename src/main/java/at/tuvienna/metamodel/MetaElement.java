package at.tuvienna.metamodel;

import java.util.LinkedList;
import java.util.List;

/**
 * @author Andreas Gruenwald <a.gruenw@gmail.com>
 * An abstract class that contains default properties for classes, attributes, associations
 * and other meta model objects.
 */
public abstract class MetaElement {
	private List<String> comment;	
	
	public MetaElement() {
		this.comment = new LinkedList<String>();	
	}


	/**
	 * @return a list of comments is returned. Comments either are notes that have been created by
	 * the user (e.g. in UML) or they have been auto generated during transformation process to 
	 * remain information. E.g. comments are created, if an abstract class has been transformed, 
	 * because this informations cannot be stored in OWL otherwise.
	 */
	public List<String> getComment() {
		return comment;
	}

	/**
	 * @param comment set a list of comments or add a comment to a list.  
	 * Comments either are notes that have been created by the user (e.g. in UML) or 
	 * they have been auto generated during transformation process to 
	 * remain information. E.g. comments are created, if an abstract class has been transformed, 
	 * because this informations cannot be stored in OWL otherwise.
	 */
	public void setComment(List<String> comment) {
		this.comment = comment;
	}
	
	/**
	 * @return name of meta element in human readable form but with as less information as 
	 * possible (only the name, no other information).
	 */
	public abstract String getName();
}
