package at.tuvienna.metamodel;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * @author Andreas Gruenwald <a.gruenw@gmail.com>
 * Represents a metaclass that can be transfered between UML and OWL. 
 * Abstract classes and interfaces are also handled as meta classes.
 */
public class MetaClass extends MetaElement {
	private String id;
	private String name;	
	private String originalName;	
	
	private boolean abstractClass;
	private boolean interfaceClass;
	private boolean enumClass;
	private Set<MetaClass> superclasses;
	private Set<MetaClass> subclasses;
	
	private List<MetaAttribute> attributes;
	private List<MetaAssociation> associations;
	
	/**
	 * Create a new meta class, interface or abstract class.
	 * @param name the diagram-wide, unique, class name.
	 */
	public MetaClass(String name) {
		super();
		this.name=name;
		this.originalName=name;
		this.abstractClass=false;
		this.interfaceClass=false;
		this.enumClass=false;
		this.superclasses = new HashSet<MetaClass>();
		this.subclasses = new HashSet<MetaClass>();
		this.attributes = new LinkedList<MetaAttribute>();	
		this.associations = new LinkedList<MetaAssociation>();
	}
	
	/**
	 * Creates a new meta class without name. This constructor must
	 * be avoided and should only be used for automated processing,
	 * such as serialization.
	 */
	public MetaClass() {
		this("");
	}
	
	
	/**
	 * @return name of meta class, as named in modeling language.
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * @param name set the name of the meta class with the name of 
	 * the class, abstract class or interface name of the model.
	 * Also {@code originalName} is changed.
	 */
	public void setName(String name) {
		this.name = name;
		this.originalName = name;
	}
	
	/**
	 * @return {@code true}, if the class has been identified as abstract.
	 */
	public boolean isAbstractClass() {
		return abstractClass;
	}
	
	/**
	 * @param abstractClass set to {@code true}, if class has been specified as abstract.
	 */
	public void setAbstractClass(boolean abstractClass) {
		this.abstractClass = abstractClass;
	}

	/**
	 * @return a set of the class's direct super classes is returned. 
	 */
	public Set<MetaClass> getSuperclasses() {
		return superclasses;
	}
	
	/**
	 * @param superclasses set or add new super classes to {@code this} class.
	 *        E.g. parent is a (direct) class of child, while grandparent is 
	 *        no direct super class.
	 */
	public void setSuperclasses(Set<MetaClass> superclasses) {
		this.superclasses = superclasses;
	}
	
	/**
	 * @return a set of the class's direct sub classes is returned.
	 */
	public Set<MetaClass> getSubclasses() {
		return subclasses;
	}

	/**
	 * @param subclasses set or add new super classes to {@code this} class.
	 * I.e. plant is a class and tree is a subclass of it. fir tree is a subclass
	 * of tree, hence fir tree is no (direct) subclass of plant.
	 */
	public void setSubclasses(Set<MetaClass> subclasses) {
		this.subclasses = subclasses;
	}

	/**
	 * @return a temporary, unique identifier is returned for the class, if set
	 * 		   during parsing process of (UML)diagram. Most diagrams use an internal id
	 * 		   that may be useful to associate classes to several elements during
	 * 		   meta model creation process.
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id set field with unique identifier for the class. In most cases, the 
	 * 		  class name is sufficient to identify a meta class, but during parsing process
	 * 		  it may be useful, to store a (UML) diagram's internal identifier to ease association
	 * 		  process. Therefore, this field is optional and the developer of the UML parser has
	 * 		  to decide, whether to use it or leave it empty. The field does not influence the final
	 * 		  ontology.
	 */
	public void setId(String id) {
		this.id = id;
	}	
	


	/**
	 * @return name of the Class.
	 */
	public String toString() {
		return this.name;
	}
	
	/**
	 * @param transformedName the name transformed into OWL compatible format.
	 * Note, that {@code originalName} still stays the same.
	 */
	public void setTransformedName(String transformedName) {
		this.name = transformedName;
	}
	
	/**
	 * @return original name of the class. Even after transformation of name into
	 * OWL compatible characters, this returns the original model's name.
	 */
	public String getOriginalName() {
		return originalName;
	}

	/**
	 * @return {@code true}, if meta class represents an interface.
	 */
	public boolean isInterfaceClass() {
		return interfaceClass;
	}

	/**
	 * @param interfaceClass set to {@code true}, if meta class represents an interface.
	 */
	public void setInterfaceClass(boolean interfaceClass) {
		this.interfaceClass = interfaceClass;
	}
	
	/**
	 * @return {@code true}, if meta class represents an enum value (or class).
	 */
	public boolean isEnumClass() {
		return enumClass;
	}

	/**
	 * @param enumClass set to {@code true}, if meta class represents an enum value (or class).
	 */
	public void setEnumClass(boolean enumClass) {
		this.enumClass = enumClass;
	}

	/**
	 * @return a list of class attributes is returned. No methods.
	 */
	public List<MetaAttribute> getAttributes() {
		return attributes;
	}

	/**
	 * @param attributes set a list of attributes (e.g. age, first name, surname). 
	 */
	public void setAttributes(List<MetaAttribute> attributes) {
		this.attributes = attributes;
	}

	/**
	 * @return a list of associations (links to other classes).
	 */
	public List<MetaAssociation> getAssociations() {
		return associations;
	}
	
	/**
	 * set or add new associations to a class - either bidirectional
	 * or unidirectional. Unidirectional associations are only reached
	 * from one meta class.
	 * @param associations list of associations
	 */
	public void setAssociations(List<MetaAssociation> associations) {
		this.associations = associations;
	}	
	
	/**
	 * find an attribute based on its name and return it.
	 * @param attributeName the name of the attribute (case sensitive)
	 * @return the attribute or {@code null}, if it does not exist.
	 */
	public MetaAttribute findAttributeByName(String attributeName) {
		for(MetaAttribute a : this.attributes) {
			if(a.getName().equals(attributeName)) {
				return a;
			}
		}
		return null;
	}

	/**
	 * find an association based on the name of the class it links to and return it.
	 * @param associationName the name of the association (case sensitive)
	 * @return the association or {@code null}, if it does not exist.
	 */
	public MetaAssociation findAssociationByName(String associationName) {
		for(MetaAssociation a : this.associations) {			
			if(a.getTo().getMetaClass().getName().equals(associationName)) {
				return a;
			}
		}
		return null;
	}

	/**
	 * removes an association from the class.
	 * @param association the association to be removed.
	 */
	public void removeAssociation(MetaAssociation association) {
		for(int i = 0; i < this.associations.size(); i++) {
			MetaAssociation a = this.associations.get(i);
			if(a == association) {
				this.associations.remove(i);
				return;
			}
		}
	}
}
