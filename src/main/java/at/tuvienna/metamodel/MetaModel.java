package at.tuvienna.metamodel;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
/**
 * @author Andreas Gruenwald <a.gruenw@gmail.com>
 * Represents the global model that contains all packages and hence all 
 * classes and constructs of a  UML class model or an Ontology and 
 * is used to transfer between UML and OWL (or reverse). 
 */
public class MetaModel {
	private String name;
	private String description;
	private Set<MetaPackage> packages;
	
	/**
	 * Create the meta model. Usually, for each meta model one OWL ontology 
	 * is created (into a separate file). One meta model is designed to contain
	 * one UML package. 
	 * @param name the name of the meta model (descriptive), if existing.
	 */
	public MetaModel(String name) {
		this.packages=new HashSet<MetaPackage>();
		this.name = name;
		this.description = "";
	}
	
	/**
	 * Creates a new meta model without name. This constructor must
	 * be avoided and should only be used for automated processing,
	 * such as serialization.
	 */
	public MetaModel() {
		this("");
	}
	
	
	/**
	 * @return name of the meta model or empty if none was existing.
	 */
	public String getName() {
		return name;
	}

	/**
	 * set the name of the meta model (descriptive). avoid special characters.
	 * Blanks are allowed.
	 * @param name the name of the meta model.
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return additional description if available.
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description set the description, if available. 
	 * 		  Anything is allowed.
	 */
	public void setDescription(String description) {
		this.description = description;
	}



	/**
	 * @return returns a set of all packages inside the meta model.
	 */
	public Set<MetaPackage> getPackages() {
		return packages;
	}
	
	/**
	 * find a meta class by its identifier (internal) an return it.
	 * @param classId unique class id (!= class name).
	 * @return the meta class with the given class id or {@code null},
	 * if no meta class matched.
	 */
	public MetaClass findClassById(String classId) {
		Iterator<MetaPackage> it = this.packages.iterator();
		while(it.hasNext()) {
			MetaPackage p = it.next();
			MetaClass metaClass = p.findClassById(classId);
			if(metaClass != null) {
				return metaClass;
			}
		}
		return null;
	}

	/**
	 * @return number of class elements that are contained in the whole meta model. 
	 */
	public int getSize() {
		int size = 0;
		Iterator<MetaPackage> it = this.packages.iterator();
		while(it.hasNext()) {
			MetaPackage p = it.next();
			size = size + p.getClasses().size();
		}
		return size;
	}
	
	/**
	 * find a meta package by its name and return it.
	 * @param packageName name of the package, case sensitive.
	 * @return meta package or {@code null}, if package does not exist.
	 */
	public MetaPackage findPackageByName(String packageName) {
		Iterator<MetaPackage> it = this.packages.iterator();
		while(it.hasNext()) {
			MetaPackage p = it.next();
			if(p.getName().equals(packageName)) {
				return p;
			}
		}
		return null;
	}

	/**
	 * find an association based on its ID. Should not be used to
	 * often, because this method is computational expensive.
	 * @param associationId name of the association ID
	 * @return list of found meta associations. In case of unidirectional relationship,
	 * 		   two associations are found. If ID is unknown, an empty list is returned.
	 */
	public List<MetaAssociation> findAssociationById(String associationId) {
		List<MetaAssociation> list = new LinkedList<MetaAssociation>();		
		Iterator<MetaPackage> it = this.packages.iterator();
		while(it.hasNext()) {
			MetaPackage p = it.next();
			for(MetaClass metaClass : p.getClasses()) {
				for(MetaAssociation ass : metaClass.getAssociations()) {
					if(ass.getAssociationId().equals(associationId)) {
						list.add(ass);
						if(ass.getInverseAssociation() != null 
						&& ass.getInverseAssociation().getAssociationId().equals(associationId)) {
							list.add(ass.getInverseAssociation());
							return list;
						}
					}
				}
			}
		}
		return list;
	}
	
	/**
	 * same as {@code findPackagByName}, but creates a new package and
	 * returns it, if package is not already existing.
	 * @param packageName name of the package, case sensitive.
	 * @return meta package; either existing one or a new one with empty class set.
	 */
	public MetaPackage managePackageByName(String packageName) {
		Iterator<MetaPackage> it = this.packages.iterator();
		while(it.hasNext()) {
			MetaPackage p = it.next();
			if(p.getName().equals(packageName)) {
				return p;
			}
		}
		MetaPackage newPackage = new MetaPackage(packageName);
		this.packages.add(newPackage);
		return newPackage;
	}

	
	/**
	 * @return whole model is returned as a string containing package, class and 
	 * attribute information.
	 */
	public String toString() {
		String s = "";
		Iterator<MetaPackage> it = this.packages.iterator();
		while(it.hasNext()) {
			MetaPackage pack = it.next();
			s += "================= Package: " + pack.getName() + "=================\n";
			s += pack.toString();
		}
		return s;
	}
}
