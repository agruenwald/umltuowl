package at.tuvienna.utils;

import java.util.Comparator;


/**
 * @author Andreas Gruenwald <a.gruenw@gmail.com>
 * Range comparator for associations. Sorts list ascending: blank = lowest, 0=higher,
 * 1,2,3,..* (* = highest).
 */
public class RangeComparator implements Comparator<String> {		
	public int compare(String value1, String value2) {
		if (value1.equals(value2)) {
			return 0;
		} else if (value1.equals("*")) {
			return 1;
		} else if (value2.equals("*")) {
			return -1;
		} else if (value1.equals("")) {
			return -1;
		} else if (value2.equals("")) {
			return 1;
		} else {
			Integer i1 = Integer.parseInt(value1);
			Integer i2 = Integer.parseInt(value2);
			if(i1 < i2) {
				return -1;
			} else  {
				return 1;
			}
		}
	}
}
