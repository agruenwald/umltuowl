package at.tuvienna.utils;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * @author Andreas Gruenwald <a.gruenw@gmail.com>
 * Utility class for reading properties from default properties file to specify different settings.
 */
public class SettingsReader {	
	private final static String SETTINGS = "settings.properties";
	private static Properties settings = null;
	private static Properties userSettings = new Properties();
	private static Map<String,String> userInput = new HashMap<String,String>();
	
	/**
	 * @param key the property key 
	 * @return the value for the given property or empty value, if property does not exist.
	 */
	public static String readProperty(String key) {
		if(userInput.get(key) != null && !userInput.get(key).isEmpty()) {
			return userInput.get(key);
		}
		return userSettings.isEmpty() ? settings.getProperty(key, "") : userSettings.getProperty(key,"");
	}
	
	/**
	 * @param key name of the property.
	 * @return {@code true}, if property is existing (that means, that name must be known in default properties file).
	 */
	public static boolean containsProperty(String key) {
		return settings.containsKey(key);
	}
	
	/**
	 * Overwrite property or set a new value.
	 * @param key name of the property
	 * @param value the value of the property
	 */
	public static void overwriteProperty(String key, String value) {
		userInput.put(key, value);
	}
	
	/**
	 * @param mySettings if not empty, the user's properties file is used for overriding the default 
	 * settings.
	 * @throws IOException if file could not be found.
	 */
	public static void loadUserSettings(String mySettings) throws IOException {		
		userSettings = new Properties();
		if(!mySettings.isEmpty()) {
			String path = mySettings;
			InputStream in = null;
			try {
				in = new FileInputStream(new File(path)); 
			} catch (IOException e) {
				throw new IOException(String.format("%s not found. Make shure, the file is located at %s.",mySettings, path));
			}
				//ClassLoader.getSystemResourceAsStream(path);
			/*
			if(in == null) {
				
			}*/
			userSettings.load(in);
			in.close();		
		}
	}
	/**
	 * Loads global settings from properties file.
	 * @throws IOException if file could not be found.
	 */
	public static void loadSettings() throws IOException {
		settings = new Properties();
		String path = SETTINGS;
		InputStream in = ClassLoader.getSystemResourceAsStream(path);
		if(in == null) {
			throw new IOException(String.format("%s not found. Make shure, the file is located in your classpath.",SETTINGS));
		}
		settings.load(in);
		in.close();			
	}
	
}
