package at.tuvienna.utils;

/**
 * 
 * @author Andreas Gruenwald <a.gruenw@gmail.com>
 * Exception is used to transport and output transformation failures in general.
 */
public class TransformationException extends Exception {
	private static final long serialVersionUID = -8057983444075567034L;

	public TransformationException(String msg) {
		super(msg);
	}

}
