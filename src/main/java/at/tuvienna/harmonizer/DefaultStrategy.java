package at.tuvienna.harmonizer;

import java.util.HashSet;
import java.util.List;
import java.util.Set;


import org.apache.log4j.Logger;

import at.tuvienna.metamodel.MetaAttribute;
import at.tuvienna.metamodel.MetaClass;
import at.tuvienna.metamodel.MetaModel;
import at.tuvienna.metamodel.MetaPackage;
import at.tuvienna.utils.SettingsReader;
import at.tuvienna.utils.Util;


/**
 * @author Andreas Gruenwald 
 * Realizes default strategy: Each package is kept separated. Class names and 
 * attributes can be pre- or/and postfixed. Different settings can be applied
 * to whether add strings to all attributes or only to those which are dupli-
 * cates.
 */
public class DefaultStrategy extends Strategy {
	private final static Logger logger = Logger.getLogger(DefaultStrategy.class);
	
	@Override
	public MetaModel applyStrategy(MetaModel metamodel) {
		String strategy = SettingsReader.readProperty("default-attribute-strategy");
		logger.info(String.format("Apply default strategy (%s).",strategy));
		for (MetaPackage pckg : metamodel.getPackages()) {
			Set<String> attrHeap = new HashSet<String>();
			List<MetaClass> pckgClasses = pckg.findAllClassesSortedByAbstract();			
			for (MetaClass metaClass : pckgClasses) {
				//transform class names if not done yet.							
				metaClass.setTransformedName(transformLocalClassName(metaClass));
				
				for(MetaAttribute ma : metaClass.getAttributes()) {
					String aName = Util.metaNameConvertion(ma.getName(),"","",true);
					if(strategy.equalsIgnoreCase("all")) {
						
						aName = this.transformLocalAttributeName(
								aName,"default-attribute-prefix","default-attribute-postfix",
								metaClass,pckg,true);
						
						
					} else if (strategy.equalsIgnoreCase("none")) {
						
						//to nothing
						
					} else if (strategy.equalsIgnoreCase("duplicates") || strategy.isEmpty()) {
						
						if(attrHeap.contains(aName)) {							
							aName = this.transformLocalAttributeName(
									aName,"default-attribute-prefix","default-attribute-postfix",
									metaClass,pckg,true);
							logger.info(String.format("Replaced attribute %s by %s for class %s in package %s.",ma.getName(),aName,metaClass,pckg.getName()));
						}
						
					} else {
						logger.error(String.format("Invalid configuration parameter %s.","default-attribute-strategy"));
						break;
					}
					ma.setName(aName);
					attrHeap.add(aName);
				}
			}
		}
		return metamodel;
	}

}
