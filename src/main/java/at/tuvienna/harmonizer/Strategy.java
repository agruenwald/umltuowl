package at.tuvienna.harmonizer;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;

import at.tuvienna.metamodel.MetaAssociation;
import at.tuvienna.metamodel.MetaClass;
import at.tuvienna.metamodel.MetaModel;
import at.tuvienna.metamodel.MetaPackage;
import at.tuvienna.utils.SettingsReader;
import at.tuvienna.utils.Util;


/**
 * @author Andreas Gruenwald 
 * Contains a strategy for converting meta model's packages, classes and attributes 
 * so that identifiers are unique and resulting OWL will fit for specified purpose.
 * Depends on property settings.
 */
public abstract class Strategy {	
	private final static Logger logger = Logger.getLogger(Strategy.class);
	/**
	 * Contains the strategy for reconstructing the meta model. E.g. Merging,
	 * naming of packages, classes, attributes depending on strategy and on
	 * parameter settings (property file, etc.).
	 * @param metaModel the global meta model.
	 * @return modified meta model
	 */
	public abstract MetaModel applyStrategy(MetaModel metaModel);
	
	/**
	 * Transform a class name depending on its type and depending on configuration settings.
	 * Package name is not considered yet.
	 * @param metaClass the meta class whom's name should be converted.
	 * @return new class name that is usually compatible for OWL.
	 */
	protected String transformLocalClassName(MetaClass metaClass) {
		String type = "";
		if(metaClass.isAbstractClass()) {
			type = "abstract";
		} else if (metaClass.isInterfaceClass()) {
			type = "interface";
		} else {
			type = "concrete";
		}
		String name = Util.metaNameConvertion(metaClass.getName(), SettingsReader.readProperty(type+"-prefix"), SettingsReader.readProperty(type+"-postfix"), true);
		return name;
	}
	
	/**
	 * Transformation of an attribute.
	 * @param aName original name of the attribute
	 * @param prefix property parameter name of setting file (property file) that contains string for prefix format.
	 * @param postfixproperty parameter name of setting file (property file) that contains string for postfix format.
	 * @param metaClass the class the attribute belongs to.
	 * @param metaPackage name of the package.
	 * @param specialChars {@code true}, if special characters should be replaced inside the name (for OWL compatiblity).
	 * @return
	 */
	protected String transformLocalAttributeName(String aName, String prefix, String postfix, MetaClass metaClass, MetaPackage metaPackage, boolean specialChars) {						
		aName = Util.metaNameConvertion(aName, 
				SettingsReader.readProperty(prefix),
				SettingsReader.readProperty(postfix),false);
		aName = aName.replace("{class}",metaClass.getName());
		aName = aName.replace("{package}",metaPackage.getName());
		return aName;
	}
	
	/**
	 * Transformation of association names based on strategy settings.
	 * @param metaModel the meta model
	 * @return model containing transformed attribute names.
	 */
	protected MetaModel transformLocalRelationNames(MetaModel metaModel) {
		Set<String> heap = new HashSet<String>();
		for(MetaPackage pckg : metaModel.getPackages()) {
			for(MetaClass metaClass : pckg.getClasses()) {
				List<MetaAssociation> removedAssocs = new ArrayList<MetaAssociation>();
				for(MetaAssociation relation : metaClass.getAssociations()) {
					
					//Special Use-Case for CDL-Flex
					if(Boolean.parseBoolean(SettingsReader.readProperty("convert-uni-to-bidirectional"))) {
						if(relation.getInverseAssociation() != null
						&& relation.getTo().getRange().size() == 1 && relation.getTo().getRange().get(0).equals("1") 
						//&& Util.isGreatherOrEqualThan(relation.getTo().getRange().get(0),1)
						&& ((!Util.isMinLimit(relation.getFrom().getRange()) || !Util.isMaxLimit(relation.getFrom().getRange()) 
								|| Util.isGreatherOrEqualThan(relation.getFrom().getRange().get(0), 2)
								|| (relation.getFrom().getRange().size() != 1) 							
								)) 						
						) {						
							relation.getInverseAssociation().setInverseAssociation(null);						
							removedAssocs.add(relation);
							continue;
						}
					}
					
					int i = 1;
					String originallyName = Util.metaNameConvertion(relation.getName(), "","",true);
					boolean noMorePartPatterns = false;
					while (true) {
						//look for patterns until no more patterns are existing in settings file
						String pattern = SettingsReader.readProperty("relation-strategy-"+i);
						if(relation.getInverseAssociation() != null && !noMorePartPatterns) {
							String partPattern = "";
							if(relation.getInverseAssociation().isAggregation()) {
								partPattern = SettingsReader.readProperty("relation-strategy-aggregation-part-"+i);
							} else if (relation.getInverseAssociation().isComposition()) {
								partPattern = SettingsReader.readProperty("relation-strategy-composition-part-"+i);
							}
							if(partPattern.isEmpty()) {
								noMorePartPatterns=true;
								continue;
							} else {
								pattern = partPattern;
							}
						}
						
						if(pattern.isEmpty()) {
							break;
						}
						String aName = pattern;
						aName = aName.replace("{package}",metaClass.getName());
						aName = aName.replace("{class}",metaClass.getName());
						aName = aName.replace("{from}",relation.getFrom().getMetaClass().getName());
						aName = aName.replace("{to}",relation.getTo().getMetaClass().getName());					
						aName = aName.replace("{name}",relation.getName());											
						
						if(aName.contains("{dependingRelation}")) {
							if(relation.isAggregation()) {
								aName = aName.replace("{dependingRelation}",SettingsReader.readProperty("relation-strategy-aggregation"));
							} else if (relation.isComposition()) {
								aName = aName.replace("{dependingRelation}",SettingsReader.readProperty("relation-strategy-composition"));
							} else {
								aName = aName.replace("{dependingRelation}",SettingsReader.readProperty("relation-strategy-relation"));
							}
						}
						aName = Util.metaNameConvertion(aName, "", "", true);
						/* Special case: user can define that named associations will not be replaced
						 * (multiple occurance allowed).
						 */
						boolean ok = false;						
						/* merging associations with same labels it not as easy as it seems...  */
						if(pattern.contains("{name}") && !relation.getName().isEmpty()
						&& SettingsReader.readProperty("allow-multiple-labeled-names")
						.equalsIgnoreCase("true")) {
							if(relation.getFrom()!=null && relation.getTo() != null
							&& (relation.getFrom().getMetaClass().getId().equals(relation.getTo().getMetaClass().getId())
							&& !(relation.isAggregation() || relation.isComposition())
							&& (!SettingsReader.readProperty("relation-own-class1-prefix").isEmpty() 
							|| !SettingsReader.readProperty("relation-own-class1-postfix").isEmpty() 
							|| !SettingsReader.readProperty("relation-own-class2-prefix").isEmpty()
							|| !SettingsReader.readProperty("relation-own-class2-postfix").isEmpty()))){
								String x = aName;
								aName=String.format("%s%s%s",SettingsReader.readProperty("relation-own-class1-prefix"),aName,SettingsReader.readProperty("relation-own-class1-postfix"));
								if(heap.contains(aName)) {
									aName = x;
									aName=String.format("%s%s%s",SettingsReader.readProperty("relation-own-class2-prefix"),aName,SettingsReader.readProperty("relation-own-class2-postfix"));									
								}
							}
							ok = true;
						} else {												
							int number = 1;
							if(aName.contains("{number}")) {
								String tmpName = aName.replace("{number}", "");
								if(!heap.contains(tmpName)) {
									aName = tmpName;
									ok = true;
								} else if (!aName.isEmpty()){
									for(int j = number; j < 10000; j++) {
										tmpName = aName.replace("{number}",String.valueOf(j));
										if(!heap.contains(tmpName)) {
											ok = true;
											aName = tmpName;
											break;
										}
									}
								}
							}
						}
						
						if (!heap.contains(aName) && !aName.isEmpty()) { //unique name
							ok = true;							
						}
						
						if (ok) {
							heap.add(aName);
							if(!originallyName.isEmpty() && !aName.equals(originallyName)) {
								relation.getComment().add(String.format("Originally, association was named \"%s\".",relation.getName()));
							}
							relation.setName(aName);
							break;
						}
						i++;
					}
				}
				
				//remove associations
				for (MetaAssociation rem : removedAssocs) {
					rem.getFrom().getMetaClass().removeAssociation(rem);
					logger.info(String.format("Removed association %s.",rem));
				}
			}
		}
		return metaModel;
	}
}
