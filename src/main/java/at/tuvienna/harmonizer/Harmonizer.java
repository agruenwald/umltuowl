package at.tuvienna.harmonizer;


import org.apache.log4j.Logger;

import at.tuvienna.metamodel.MetaModel;
import at.tuvienna.utils.SettingsReader;


/**
 * @author Andreas Gruenwald 
 * Guarantee uniqueness of class, attribute names.
 * Merge all classes into a single metamodel or separate them into different
 * metamodels depending on settings.
 */
public class Harmonizer {
	private final static Logger logger = Logger.getLogger(Harmonizer.class);

	/**
	 * Harmonize components of meta model by merging, proof name uniqueness, etc.
	 * depending on parameter settings an return harmonized metamodel.
	 * @param metamodel original model gained from UML file (or other source).
	 * @return harmonized meta model, compatible for OWL.
	 */
	public MetaModel harmonize(MetaModel metamodel) {
		//1) Merge packages (also handles class names)
		boolean mergePackages = Boolean.parseBoolean(SettingsReader.readProperty("merge-packages"));
		Strategy strategy = null;
		if(mergePackages) {
			strategy = new MergeStrategy();
		}	
		else {
			strategy = new DefaultStrategy();
		}					
		metamodel = strategy.applyStrategy(metamodel);
		metamodel = strategy.transformLocalRelationNames(metamodel);
		
		logger.debug(metamodel);
		return metamodel;
	}
}